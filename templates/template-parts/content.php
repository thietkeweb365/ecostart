<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 */

$class  = 'col-md-' . ( 12 / $column );

?>
<article id="post-<?php the_ID(); ?>" <?php post_class( $class ); ?>>
    <div class="row">
        <div class="col-md-4">
            <?php thim_feature_image( 275, 160, 'full' ); ?>
        </div>

        <div class="col-md-8">
            <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
            <?php thim_entry_meta(); ?>
            <div class="entry-summary">
                <?php
                printf('%1$s', get_excerpt(get_the_excerpt(), 29, '...'));
                ?>
            </div><!-- .entry-summary -->
            <a href="<?php echo esc_url( get_permalink() ); ?>" class="btn btn-xs btn-primary btn-readmore"><?php echo esc_html__( 'Read More', 'hairsalon' ); ?></a>
        </div>

    </div>
</article><!-- #post-## -->
