<?php
/**
 * Template part for displaying single.
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 *
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="content-inner">
		<div class="entry-content">
			<header class="entry-header">
				<?php
				if ( is_single() ) {
					the_title( '<h1 class="heading__primary entry-title"><span class="inline-title">', '</span><span class="line"></span></h1>' );
				}
				?>
			</header><!-- .entry-header -->

			<?php thim_entry_meta(); ?>

			<div class="entry-description">
				<?php

				// Get post content
				the_content();

				wp_link_pages(
					array(
						'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'hairsalon' ),
						'after'  => '</div>',
					)
				);
				?>
			</div>

			<div class="entry-tag-share">
				<?php do_action( 'thim_social_share' ); ?>
			</div>

		</div><!-- .entry-content -->

	</div><!-- .content-inner -->


</article><!-- #post-## -->