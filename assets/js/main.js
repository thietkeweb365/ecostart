jQuery(document).ready( function($){



	$(document).on('click', '.hamburger', function(){
		if($(this).hasClass("is-active")){
			$('#menu-main-menu').slideUp();
			$(this).removeClass("is-active")

		}else{
			$('#menu-main-menu').slideDown();
			$(this).addClass("is-active");
		}
	});



	$(document).on('click', '.bh-minus-menu', function(){
		var $li = $(this).closest('li');
		$('.menu-item-has-children').not($li).removeClass('open');
		$('.menu-item-has-children').not($li).find('.sub-menu').slideUp();

		if($li.hasClass('open')){
			$li.removeClass('open');
			$li.find('.sub-menu').slideUp();
		}else{
			$li.addClass('open');
			$li.find('.sub-menu').slideDown();
		}


		return false;
	});
	$('.qty-changer').on('click', '.qty_dec', function(e) {

		$input = $(this).closest('.qty-holder').find('input.qty');
		$max = $input.attr('max');

		var val = parseInt($input.val());
		var step = $input.attr('step');
		step = 'undefined' !== typeof(step) ? parseInt(step) : 1;

		if($max && (val + step) <= $max){
            $input.val( val + step ).change();
		}


	});

	$('.qty-changer').on('click', '.qty_inc', function(e) {
		$input = $(this).closest('.qty-holder').find('input.qty');
		var val = parseInt($input.val());
		var step = $input.attr('step');
		step = 'undefined' !== typeof(step) ? parseInt(step) : 1;
		if (val > 1) {
			$input.val( val - step ).change();
		}
	});


    resize_func();
    $( window ).resize(function() {
        resize_func();
    });
    if(jQuery().etalage){
        $('#etalage').etalage({
            thumb_image_width: 375,
            thumb_image_height: 350,
            source_image_width: 900,
            source_image_height: 1200
        });
    }
	function resize_func(){
        var $width = $(window).width();
        if($width < 992){
            $(document).on('click', '.menu-item-has-children > a', function(){
                $(this).closest('li').find('.sub-menu').slideToggle();
                return false;
            });
        }
        $( ".heading__primary" ).each(function( index ) {
            var $txt_width = $(this).find('.inline-title').width() + 15;
            var $heading_width = $(this).width() - $txt_width;
            $(this).find('.line').css({'left' : $txt_width, 'width' : ($heading_width - 20)});

        });

	}


    $('[data-tooltip=""]').tooltip();
});
