<?php
/**
 * thim functions and definitions
 *
 * @package thim
 */
$m_themes       = wp_get_theme();
$text_domain    = $m_themes->get( 'TextDomain' );
define( 'BH_TEXTDOMAIN', $text_domain);

define( 'BH_DIR', trailingslashit( get_template_directory() ) );
define( 'BH_URI', trailingslashit( get_template_directory_uri() ) );

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'thim_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function thim_setup() {

		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on thim, use a find and replace
		 * to change 'landscaping' to the name of your theme in all the template files
		 */
		load_theme_textdomain( 'landscaping', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'woocommerce' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
		 */
		add_theme_support( 'post-thumbnails' );
		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'primary' => esc_html__( 'Primary Menu', 'landscaping' ),
			'footer' => esc_html__( 'Footer Menu', 'landscaping' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		/*
		 * Enable support for Post Formats.
		 * See http://codex.wordpress.org/Post_Formats
		 */
		add_theme_support( 'post-formats', array(
			'aside',
			'image',
			'video',
			'quote',
			'link',
			'gallery',
			'audio'
		) );

		add_theme_support( "title-tag" );
		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'thim_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

	}

endif; // thim_setup
add_action( 'after_setup_theme', 'thim_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */

function BH_Widgets_init() {
	$theme_options_data = get_theme_mods();

    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar', 'landscaping' ),
        'id'            => 'sidebar',
        'description'   => esc_html__( 'Hiển thị sidebar chung (trừ trang chủ)', 'landscaping' ),
        'before_widget' => '<div class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="widget-title no-box">',
        'after_title'   => '</h3>',
    ) );
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar of Products', 'landscaping' ),
		'id'            => 'sidebar-product',
		'description'   => esc_html__( 'Hiển thị sidebar ở trang chi tiết sản phẩm.', 'landscaping' ),
		'before_widget' => '<div class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer', 'landscaping' ),
		'id'            => 'footer',
		'description'   => esc_html__( 'Display on footer', 'landscaping' ),
		'before_widget' => '<div id="%1$s" class="%2$s col-md-4 footer-widgets">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

}

add_action( 'widgets_init', 'BH_Widgets_init' );



/**
 * Enqueue scripts and styles.
 */
function thim_scripts() {
    wp_enqueue_style( BH_TEXTDOMAIN . '-bootstrap', get_template_directory_uri() .'/assets/css/bootstrap.min.css', array(), '1.0.0' );
    wp_enqueue_style( BH_TEXTDOMAIN . '-font-awesome', get_template_directory_uri() .'/assets/css/font-awesome.min.css', array(), '4.6.3' );
    if(is_product()){
        wp_enqueue_style( BH_TEXTDOMAIN . '-etalage', get_template_directory_uri() .'/assets/css/etalage.css', 'all' );
    }

    wp_enqueue_style( BH_TEXTDOMAIN . '-main', get_template_directory_uri() .'/assets/css/style.css', 'all' );



    wp_enqueue_script( BH_TEXTDOMAIN . '-tether-js', get_template_directory_uri() . '/assets/js/tether.min.js', array(), '1.0.0', true );
    wp_enqueue_script( BH_TEXTDOMAIN . '-bootstrap-js', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array(), '1.0.0', true );

    if(is_product()){
        wp_enqueue_script( BH_TEXTDOMAIN . '-etalage-js', get_template_directory_uri() . '/assets/js/jquery.etalage.min.js', array(), '1.0.0', true );
    }

    wp_enqueue_script( BH_TEXTDOMAIN . '-theme', get_template_directory_uri() . '/assets/js/main.js', array(), '', true );


}

add_action( 'wp_enqueue_scripts', 'thim_scripts' );

function thim_custom_admin_scripts() {
	wp_enqueue_style( 'thim-custom-admin', BH_URI . 'assets/css/custom-admin.css', array() );
}

add_action( 'admin_enqueue_scripts', 'thim_custom_admin_scripts' );


function thim_show_script_id( $tag, $handle, $src ) {
	$tag = str_replace( '<script', '<script id="' . $handle . '"', $tag );

	return $tag;
}

add_filter( 'script_loader_tag', 'thim_show_script_id', 100, 3 );






// require
require BH_DIR  . 'inc/custom-functions.php';

/**
 * Custom template tags for this theme.
 */
require BH_DIR  . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require BH_DIR  . '/inc/extras.php';
require BH_DIR  . 'inc/aq_resizer.php';

/**
 * Customizer additions.
 */

require BH_DIR  . 'inc/woocommerce.php';

if ( thim_plugin_active( 'bh-framework/tp-framework.php' ) ) {

	require BH_DIR  . 'inc/widgets/widgets.php';

	require BH_DIR  . 'inc/admin/customize-options.php';

	require BH_DIR  . 'inc/tax-meta.php';
}



//pannel Widget Group
function bh_widget_group( $tabs ) {
	$tabs[] = array(
		'title'  => esc_html__( 'Thim Widget', 'landscaping' ),
		'filter' => array(
			'groups' => array( 'bh_widget_group' )
		)
	);

	return $tabs;
}

add_filter( 'siteorigin_panels_widget_dialog_tabs', 'bh_widget_group', 19 );




    function set_thumbnail($post_id, $size = FALSE, $default = FALSE)
    {
        global $post;
        if($size){
            $thumb_src = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), $size );
            $image_alt = get_the_title( get_post_thumbnail_id($post_id) );
        }else{
            $thumb_src = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'full' );
            $image_alt = get_the_title( get_post_thumbnail_id($post_id) );
        }

        $thumb_src = $thumb_src[0];

        if(empty($thumb_src) ){
            $post = get_post($post_id);
            $thumb_src = get_thumbail_content($post->post_content, '', get_template_directory_uri().'/'.$default);
            $image_alt = '';
        }

        return array('src' => $thumb_src, 'alt' => $image_alt);
    }
    function get_thumbail_content($post_content, $format = null, $thumbnail_default) {
        $first_img = '';
        ob_start();
        ob_end_clean();
        $post_content = do_shortcode($post_content);
        $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post_content, $matches);
        $first_img = $matches[1][0];

        if(empty($first_img)) {
            $first_img = $thumbnail_default;
        }
        return $first_img;
    }

    function formatMoney($number, $dotma = NULL, $fractional=false) {  
        if ($fractional) {  
            $number = sprintf('%.2f', $number);  
        } 

        while (true) {  
            $replaced = preg_replace('/(-?\d+)(\d\d\d)/', '$1'.$dotma.'$2', $number);  
            if ($replaced != $number) {  
                $number = $replaced;  
            } else {  
                break;  
            }  
        }  
        return $number;  
    }

    function mang($mang){
    	echo '<pre>';
    	print_r($mang);
    	echo '</pre>';
    }

    function get_excerpt($content, $num, $short){        

        // filter_content
        $content = trim(strip_tags($content));
        $content = preg_replace('|[[\\/\\!]*?[^\\[\\]]*?]|si', '', $content);
        $content = preg_replace("/[\r\n]*/","",$content);
        mb_internal_encoding('utf-8');
        $content = str_replace('  ', '', $content);

        $limit = $num - 1 ;
        $str_tmp = '';
        $arrstr = explode(" ", $content);
        if ( count($arrstr) <= $num ) {
            if (strlen($content)>(9*$num))
                return substr($content,0,(9*$num)).$short;
            else
                return $content; 
        }
        if (!empty($arrstr))
        {
            for ( $j=0; $j< count($arrstr) ; $j++)
            {
                $str_tmp .= " " . $arrstr[$j];
                if ($j == $limit)
                {  break; }
            }
        }
        if (strlen($str_tmp)>9*$num)
            {
                return substr($str_tmp,0,9*$num).$short;
            }

        return $str_tmp.$short;
    }

    add_filter('request', 'category_name');

    function category_name($request){
        if( isset($request['category_name']) ){
            $request['posts_per_page'] = 1;
        }
        return $request;
    }
    function get_src($id, $size = 'thumbnail')
    {
    	$src = wp_get_attachment_image_src($id, $size);
    	if(isset($src[0])){
    		return $src[0];
    	}
    }

    function rambo_terms($parent = 0)
    {
		global $wpdb;
		$terms = $wpdb->get_results( "SELECT * FROM $wpdb->terms LEFT JOIN $wpdb->term_taxonomy ON $wpdb->terms.term_id = $wpdb->term_taxonomy.term_id WHERE $wpdb->term_taxonomy.taxonomy = 'product_cat' AND $wpdb->term_taxonomy.parent = '".$parent."'", OBJECT );
		return $terms;
    }

	add_filter( 'wp_pagenavi', 'ik_pagination', 10, 2 );
	function ik_pagination($html) {
		$html = str_replace('<a','<li><a',$html); 
		$html = str_replace('</a>','</a></li>',$html); 

		if (preg_match('#<span class=\'extend\'[^<>]*>...</span>#', $html, $matches_ex)) {
			$html = str_replace($matches_ex[0],'<li class="extend"><a href="#">...</a></li>',$html); 
		}
		if (preg_match("/<div class=\'wp\-pagenavi\'>(.*?)<\/div>/s", $html, $div_black)) {
			$html = str_replace($div_black[0],'<div class="pagination loop-pagination pagination-center"><ul class="page-numbers">'.$div_black[1].'</ul></div>',$html);
		}
		if (preg_match("/<span class='pages'>(.*?)<\/span>/s", $html, $pages)) {
			$html = str_replace($pages[0],'',$html); 
		}
		if (preg_match('#<span class=\'current\'[^<>]*>([\d,]+).*?</span>#', $html, $matches)) {
		   $html = str_replace($matches[0],'<li><span class="page-numbers current">'.$matches[1].'</span></li>',$html);
		}

	    return $html;
	}

add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 24;' ), 20 );


