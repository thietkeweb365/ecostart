<?php
get_header();
$home = get_post(get_option( 'page_on_front' ));
$cat = get_queried_object();?>
    <div class="breadcrumbs-wrap">
        <div class="container">
            <ol itemscope="" itemtype="http://schema.org/BreadcrumbList">
                <li><a href="<?php echo home_url();?>"><?php echo $home->post_title;?></a></li>&nbsp;/&nbsp;
                <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a href="<?php echo get_category_link($cat->term_id);?>" itemscope="" itemtype="http://schema.org/Thing" itemprop="item"><span itemprop="name"><?php echo $cat->name;?></span></a><meta itemprop="position" content="1"></li>
            </ol>
        </div>
    </div>
    <div class="container blog" id="content">
        <div class="row">
            <div class="col-md-9">
                <h1 class="heading__primary"><span class="inline-title"><?php echo $cat->name;?></span><span class="line"></span></h1>
                <div class="blog-posts">
                    <?php
                    $posts_per_page = get_option( 'posts_per_page' );
                      $query_args = array(
                        'post_status' => 'publish',
                        'post_type' => 'post',
                        'posts_per_page' => $posts_per_page,
                        'orderby' => 'date',
                        'order' => 'DESC',
                        'tax_query' => array(
                            array(
                              'taxonomy' => 'category',
                              'field' => 'term_id',
                              'terms' => $cat->term_id,
                            )
                          ),
                          'paged' => max($paged, $page)
                      );

                      $query_posts = new WP_Query($query_args);
                      if( $query_posts->have_posts() ){
                        while ($query_posts->have_posts()) : $query_posts->the_post();
                            get_template_part( 'templates/template-parts/content' );
                        endwhile;
                      }
                        wp_reset_postdata();
                      ?>
                </div>
                <?php wp_pagenavi( array( 'query' => $query_posts ) ); ?>
            </div>
            <?php get_sidebar();?>
        </div>
    </div>
<?php get_footer();?>