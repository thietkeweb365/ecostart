<?php

class Rambo_Support_Widget extends BH_Widget {
	function __construct() {
		parent::__construct(
			'fanpage',
			esc_html__( 'BH: Fanpage', 'blackhole' ),
			array(
				'description'   => esc_html__( 'Fanpage of Sidebar', 'blackhole' ),
				'help'          => '',
				'panels_groups' => array( 'bh_widget_group' ),
				'panels_icon'   => 'dashicons dashicons-format-image'
			),
			array(),
			array(
				'label' => array(
					'type'        => 'text',
					'default'     => '',
					'label'       => esc_html__( 'Label', 'blackhole' )
				),
				'banner' => array(
					'type'        => 'media',
					'default'     => '',
					'label'       => esc_html__( 'Banner', 'blackhole' )
				),
				'support' => array(
					'label'     => esc_html__( 'List tabs', 'blackhole' ),
					'type'      => 'repeater',
					'item_name' => esc_html__( 'Item', 'blackhole' ),
					'fields'    => array(
						'label' => array(
							'type'        => 'text',
							'label'       => esc_html__( 'Label', 'blackhole' ),
							'description' => esc_html__( 'Enter label of section', 'blackhole' )
						),
						'tel' => array(
							'type'        => 'text',
							'default'     => '',
							'label'       => esc_html__( 'Tel', 'blackhole' )
						),
						'skype' => array(
							'type'        => 'text',
							'default'     => '',
							'label'       => esc_html__( 'Skype', 'blackhole' )
						),
					)
				),
				'email' => array(
					'type'        => 'text',
					'default'     => '',
					'label'       => esc_html__( 'Email', 'blackhole' )
				)
			),
			BH_DIR  . 'inc/widgets/support'
		);
	}

	/**
	 * @param $instance
	 *
	 * @return string
	 */

	function get_template_name( $instance ) {
		return 'base';
	}

	function get_style_name( $instance ) {
		return false;
	}
	function get_term_lists()
	{
		global $wpdb;
		$terms = $wpdb->get_results( "SELECT * FROM $wpdb->terms LEFT JOIN $wpdb->term_taxonomy ON $wpdb->terms.term_id = $wpdb->term_taxonomy.term_id WHERE taxonomy = 'product_cat'", OBJECT );

		if($terms){
			$option = array();
			foreach ($terms as $key => $term) {
				$option[$term->term_id] = esc_html__( $term->name, 'blackhole' );
			}
			return $option;
		}
	}

}

function rambo_support_widget_register() {
	register_widget( 'Rambo_Support_Widget' );
}

add_action( 'widgets_init', 'rambo_support_widget_register' );


