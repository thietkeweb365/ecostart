<?php

class Rambo_Facebook_Widget extends BH_Widget {
	function __construct() {
		parent::__construct(
			'fanpage',
			esc_html__( 'BH: Fanpage', 'blackhole' ),
			array(
				'description'   => esc_html__( 'Fanpage of Sidebar', 'blackhole' ),
				'help'          => '',
				'panels_groups' => array( 'bh_widget_group' ),
				'panels_icon'   => 'dashicons dashicons-format-image'
			),
			array(),
			array(
				'label' => array(
					'type'        => 'text',
					'default'     => '',
					'label'       => esc_html__( 'Label', 'blackhole' )
				),
				'banner' => array(
					'type'        => 'media',
					'default'     => '',
					'label'       => esc_html__( 'Banner', 'blackhole' )
				),
				'support' => array(
					'label'     => esc_html__( 'List tabs', 'blackhole' ),
					'type'      => 'repeater',
					'item_name' => esc_html__( 'Item', 'blackhole' ),
					'fields'    => array(
						'label' => array(
							'type'        => 'text',
							'label'       => esc_html__( 'Label', 'blackhole' ),
							'description' => esc_html__( 'Enter label of section', 'blackhole' )
						),
						'tel' => array(
							'type'        => 'text',
							'default'     => '',
							'label'       => esc_html__( 'Tel', 'blackhole' )
						),
						'skype' => array(
							'type'        => 'text',
							'default'     => '',
							'label'       => esc_html__( 'Skype', 'blackhole' )
						),
					)
				),
				'email' => array(
					'type'        => 'text',
					'default'     => '',
					'label'       => esc_html__( 'Email', 'blackhole' )
				)
			),
			BH_DIR  . 'inc/widgets/facebook'
		);
	}

	/**
	 * @param $instance
	 *
	 * @return string
	 */

	function get_template_name( $instance ) {
		return 'base';
	}

	function get_style_name( $instance ) {
		return false;
	}

}

function rambo_facebook_widget_register() {
	register_widget( 'Rambo_Facebook_Widget' );
}

add_action( 'widgets_init', 'rambo_facebook_widget_register' );


