<?php

class Thim_Google_Map_Widget extends BH_Widget {
	function __construct() {
		parent::__construct(
			'google-map',
			__( 'BH: Google Maps', 'landscaping' ),
			array(
				'description'   => esc_html__( 'A Google Maps widget.', 'landscaping' ),
				'help'          => '',
				'panels_groups' => array( 'bh_widget_group' )
			),
			array(),
			array(
				'title'      => array(
					'type'  => 'text',
					'label' => esc_html__( 'Title', 'landscaping' ),
				),
				'map_center' => array(
					'type'        => 'textarea',
					'rows'        => 2,
					'label'       => esc_html__( 'Map center', 'landscaping' ),
					'description' => esc_html__( 'The name of a place, town, city, or even a country. Can be an exact address too.', 'landscaping' )
				),
				'settings'   => array(
					'type'        => 'section',
					'label'       => esc_html__( 'Settings', 'landscaping' ),
					'hide'        => false,
					'description' => esc_html__( 'Set map display options.', 'landscaping' ),
					'fields'      => array(
						'apikey'      => array(
							'type'  => 'text',
							'label' => esc_html__( 'API_KEY', 'landscaping' ),
						),
						'height'      => array(
							'type'    => 'text',
							'default' => 480,
							'label'   => esc_html__( 'Height', 'landscaping' )
						),
						'zoom'        => array(
							'type'        => 'slider',
							'label'       => esc_html__( 'Zoom level', 'landscaping' ),
							'description' => esc_html__( 'A value from 0 (the world) to 21 (street level).', 'landscaping' ),
							'min'         => 0,
							'max'         => 21,
							'default'     => 12,
							'integer'     => true,

						),
						'scroll_zoom' => array(
							'type'        => 'checkbox',
							'default'     => true,
							'state_name'  => 'interactive',
							'label'       => esc_html__( 'Scroll to zoom', 'landscaping' ),
							'description' => esc_html__( 'Allow scrolling over the map to zoom in or out.', 'landscaping' )
						),
						'draggable'   => array(
							'type'        => 'checkbox',
							'default'     => true,
							'state_name'  => 'interactive',
							'label'       => esc_html__( 'Draggable', 'landscaping' ),
							'description' => esc_html__( 'Allow dragging the map to move it around.', 'landscaping' )
						)
					)
				),
				'markers'    => array(
					'type'        => 'section',
					'label'       => esc_html__( 'Markers', 'landscaping' ),
					'hide'        => true,
					'description' => esc_html__( 'Use markers to identify points of interest on the map.', 'landscaping' ),
					'fields'      => array(
						'marker_at_center' => array(
							'type'    => 'checkbox',
							'default' => true,
							'label'   => esc_html__( 'Show marker at map center', 'landscaping' )
						),
						'marker_icon'      => array(
							'type'        => 'media',
							'default'     => '',
							'label'       => esc_html__( 'Marker Icon', 'landscaping' ),
							'description' => esc_html__( 'Replaces the default map marker with your own image.', 'landscaping' )
						),
						//						'markers_draggable' => array(
						//							'type'       => 'checkbox',
						//							'default'    => false,
						//							'state_name' => 'interactive',
						//							'label'      => esc_html__( 'Draggable markers', 'landscaping' )
						//						),
						'marker_positions' => array(
							'type'      => 'repeater',
							'label'     => esc_html__( 'Marker positions', 'landscaping' ),
							'item_name' => esc_html__( 'Marker', 'landscaping' ),
							//							'item_label' => array(
							//							 'selector'     => "[id*='marker_positions-place']",
							//								'update_event' => 'change',
							//								'value_method' => 'val'
							//							),
							'fields'    => array(
								'place' => array(
									'type'  => 'textarea',
									'rows'  => 2,
									'label' => esc_html__( 'Place', 'landscaping' )
								)
							)
						)
					)
				),
			)
		);
	}

	function enqueue_frontend_scripts() {
		wp_enqueue_script( 'thim-google-map', BH_URI . 'inc/widgets/google-map/js/js-google-map.js', array( 'jquery' ), true );
	}

	function get_template_name( $instance ) {
		return 'base';
	}

	function get_style_name( $instance ) {
		return false;
	}

	function get_template_variables( $instance, $args ) {
		$settings = $instance['settings'];
		$markers  = $instance['markers'];
		$mrkr_src = wp_get_attachment_image_src( $instance['markers']['marker_icon'] );
		{
			return array(
				'map_id'   => md5( $instance['map_center'] ),
				'height'   => $settings['height'],
				'map_data' => array(
					'address'          => $instance['map_center'],
					'zoom'             => $settings['zoom'],
					'scroll-zoom'      => $settings['scroll_zoom'],
					'draggable'        => $settings['draggable'],
					'marker-icon'      => ! empty( $mrkr_src ) ? $mrkr_src[0] : '',
					'api-key' => $settings['apikey'],
					//	'markers-draggable' => isset( $markers['markers_draggable'] ) ? $markers['markers_draggable'] : '',
					'marker-at-center' => $markers['marker_at_center'],
					'marker-positions' => isset( $markers['marker_positions'] ) ? json_encode( $markers['marker_positions'] ) : '',
				)
			);
		}
	}
}

//
function thim_google_map_widget() {
	register_widget( 'Thim_Google_Map_Widget' );
}

add_action( 'widgets_init', 'thim_google_map_widget' );