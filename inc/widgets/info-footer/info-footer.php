<?php

class Rambo_Info_Footer_Widget extends BH_Widget {
	function __construct() {
		parent::__construct(
			'info-footer',
			esc_html__( 'Rambo: Info Footer', 'blackhole' ),
			array(
				'description'   => esc_html__( 'Menu', 'blackhole' ),
				'help'          => '',
				'panels_groups' => array( 'bh_widget_group' ),
				'panels_icon'   => 'dashicons dashicons-format-image'
			),
			array(),
			array(
				'logo_footer' => array(
					"type"    => "media",
					"label"   => esc_html__( "Logo", 'blackhole' ),
					"options" => $this->get_menu_select(),
				),
				'text_footer' => array(
					"type"    => "editor",
					"label"   => esc_html__( "Text", 'blackhole' ),
					"options" => $this->get_menu_select(),
				)
			),
			BH_DIR  . 'inc/widgets/info-footer'
		);
	}

	/**
	 * @param $instance
	 *
	 * @return string
	 */

	function get_template_name( $instance ) {
		return 'base';
	}

	function get_style_name( $instance ) {
		return false;
	}

	function get_menu_select()
	{
		$menu = get_terms( 'nav_menu', array( 'hide_empty' => true ) );
		if($menu){
			$option = array();
			foreach ($menu as $key => $value) {
				$option[$value->term_id] = esc_html__( $value->name, 'blackhole' );
			}
			return $option;
		}
	}
}

function rambo_info_footer_widget_register() {
	register_widget( 'Rambo_Info_Footer_Widget' );
}

add_action( 'widgets_init', 'rambo_info_footer_widget_register' );


