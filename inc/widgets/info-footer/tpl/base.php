<?php
$title = $instance['title'];
$logo_footer = $instance['logo_footer'];
$text_footer = $instance['text_footer'];


?>
                        <a class="logo-footer" href="<?php echo home_url();?>"><img src="<?php echo get_src($logo_footer, 'full');?>" alt="<?php echo get_bloginfo('name');?>"></a>
                        <p><?php echo $text_footer;?></p>