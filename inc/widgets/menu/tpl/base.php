<?php
$title = $instance['title'];
$menu_select = $instance['menu_select'];
$menu_items = wp_get_nav_menu_items($menu_select);


?>

<section id="sidebar-menu" class="widget sidebar-menu">
	<div class="widget-wrap">
		<h4 class="widget-title widgettitle"><?php echo $title;?></h4>
		<ul class="menu">
			<?php
				if($menu_items){
				    foreach ($menu_items as $key => $items) {?>
				    <li id="menu-item-4264" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-4264"><a href="<?php echo $items->url;?>"><?php echo $items->title;?></a></li>
				    <?php }
				}?>
		</ul>
	</div>
</section>