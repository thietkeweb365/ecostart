<?php
$title = $instance['title'];
$menu_select = $instance['menu_select'];


?>

<aside class="widget woocommerce widget_product_categories">
	<div class="widget-wrap">
		<h3 class="widget-title"><?php echo $title;?> <span class="toggle"></span></h3>
		<?php wp_nav_menu( array( 'menu' => $menu_select, 'menu_class' => 'product-categories', 'walker' => new Nav_Walker_Nav_Menu() ) ); ?>
	</div>
</aside>