<?php

class Rambo_Category_Widget extends BH_Widget {
	function __construct() {
		parent::__construct(
			'category',
			esc_html__( 'Rambo: Category', 'blackhole' ),
			array(
				'description'   => esc_html__( 'Category of sidebar', 'blackhole' ),
				'help'          => '',
				'panels_groups' => array( 'bh_widget_group' ),
				'panels_icon'   => 'dashicons dashicons-format-image'
			),
			array(),
			array(
				'title' => array(
					'type'                  => 'text',
					'label'                 => esc_html__( 'Heading Text', 'blackhole' ),
					'default'               => '',
				),
				'menu_select' => array(
					"type"    => "select",
					"label"   => esc_html__( "Menu", 'blackhole' ),
					"options" => $this->get_menu_select(),
				)
			),
			BH_DIR  . 'inc/widgets/category'
		);
	}

	/**
	 * @param $instance
	 *
	 * @return string
	 */

	function get_template_name( $instance ) {
		return 'base';
	}

	function get_style_name( $instance ) {
		return false;
	}

	function get_menu_select()
	{
		$menu = get_terms( 'nav_menu', array( 'hide_empty' => true ) );
		if($menu){
			$option = array();
			foreach ($menu as $key => $value) {
				$option[$value->term_id] = esc_html__( $value->name, 'blackhole' );
			}
			return $option;
		}
	}
}

function rambo_category_widget_register() {
	register_widget( 'Rambo_Category_Widget' );
}

add_action( 'widgets_init', 'rambo_category_widget_register' );


class Nav_Walker_Nav_Menu extends Walker_Nav_Menu{
	function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ){
        $id_field = $this->db_fields['id'];
        if ( is_object( $args[0] ) ) {
            $args[0]->has_children = ! empty( $children_elements[$element->$id_field] );
        }
        return parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
   }
  function start_el(&$output, $item, $depth, $args){
       global $wp_query;
       $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
       $class_names = $value = '';
       $classes = empty( $item->classes ) ? array() : (array) $item->classes;
       $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
       $class_names = ' class="'. esc_attr( $class_names ) . '"';

       $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

       $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
       $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
       $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
      $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';


       $description  = ! empty( $item->description ) ? '<span>'.esc_attr( $item->description ).'</span>' : '';



        $item_output = $args->before;
        $item_output .= '<a'. $attributes .'>';
        $item_output .=$args->link_before .apply_filters( 'the_title', $item->title, $item->ID );
        $item_output .= $description.$args->link_after;
        
        if ( $args->has_children ) {
        	$item_output .= '<span class="fa fa-plus bh-minus-menu"></span>';
        }
        $item_output .= '</a>';
        $item_output .= $args->after;

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
}