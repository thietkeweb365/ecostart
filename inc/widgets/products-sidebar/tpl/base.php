<?php
global $post, $_product;
$panels_info = $instance['panels_info'];
$label = $instance['label'];
$cat = $instance['cat'];
$limit = $instance['limit'];

$cat = get_term_by('term_id', $cat, 'product_cat');


$args = array(
  'post_status' => 'publish',
  'post_type' => 'product',
  'posts_per_page' => $limit,
  'tax_query' => array(
	array(
	   'taxonomy' => 'product_cat',
	   'field' => 'ID',
	   'terms' => $cat->term_id
	)
  )
);

$query = new WP_Query($args);
	if( $query->have_posts() ){?>
    <aside class="widgets">
        <h3 class="widget-title no-box"><?php echo $label;?></h3>
        <ul class="product-list widget-product-lists">
            <?php while ($query->have_posts()) : $query->the_post();
                $img = set_thumbnail($post->ID);
                ?>
                <li class="product-item clearfix">
                    <?php do_action( 'woocommerce_before_shop_loop_item_title' );?>
                    <a class="product-loop-title" href="<?php echo get_permalink($post->ID);?>" title="<?php echo $post->post_title;?>"><h3><?php echo $post->post_title;?></h3></a>
                    <?php do_action( 'woocommerce_after_shop_loop_item_title' );?>
                </li>
            <?php endwhile;?>
        </ul>
        <!--.product-list-->
    </aside>


<?php }
wp_reset_postdata();?>
</div>
