<?php
$thim_animation = $des = $show_icon = $show_view_more = $css = $class = $no_title = '';

$show_icon = '';
$icon_heading = $instance['icon_heading'];
$custom_icon_heading = $instance['custom_icon_heading'];
if($icon_heading == 'custom'){
    $show_icon = '<i class="'.$custom_icon_heading.'"></i> ';
}

$view_more = $instance['view_more'];
$custom_view_more_label = $instance['custom_view_more_label'];
$custom_view_more_url = $instance['custom_view_more_url'];
if($view_more == 'custom'){
    $show_view_more = '<a href="'.$custom_view_more_url.'" class="heading-viewmore" title="'.$custom_view_more_label.'"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i></a> ';
}

if ( $instance['textcolor'] ) {
	$css .= 'color:' . $instance['textcolor'] . ';';
}

if ( $instance['font_heading'] == 'custom' ) {
	if ( $instance['custom_font_heading']['custom_font_size'] <> '' ) {
		$css .= 'font-size:' . $instance['custom_font_heading']['custom_font_size'] . 'px;';
	}
	if ( $instance['custom_font_heading']['custom_font_weight'] <> '' ) {
		$css .= 'font-weight:' . $instance['custom_font_heading']['custom_font_weight'] . ';';
	}
	if ( $instance['custom_font_heading']['custom_font_style'] <> '' ) {
		$css .= 'font-style:' . $instance['custom_font_heading']['custom_font_style'] . ';';
	}
}

if ( $css ) {
	$css = ' style="' . $css . '"';
}

if ( $instance['line'] ) {
	$border_color = ' style="border-color: ' . $instance['textcolor'] . '"';
	$line_heading = '<span class="line-heading" ' . $border_color . '></span>';
	$class        = ' wrapper-line-heading';
}

$show_line = ( $instance['show_line'] != false ) ? 'show_line' : '';

if ( $instance['title'] ) {
	$title = '<' . $instance['size'] . $css . ' class="heading__primary' . $class . '"><span class="inline-title">' . $show_icon.$instance['title'] .'</span><span class="line"></span>' . $show_view_more . '</' . $instance['size'] . '>';
} else {
	$no_title = ' no-title';
}
if ( $instance['sub-title'] ) {
	$subtitle = '<p class="heading__secondary"  ' . $css . ' >' . $instance['sub-title'] . '</p>';
}

echo $title;