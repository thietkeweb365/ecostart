<?php

class BH_Heading_Widget extends BH_Widget {

    function __construct() {
        parent::__construct(
            'heading',
            __( 'BH: Heading', 'landscaping' ),
            array(
                'description'   => esc_html__( 'Add heading text', 'landscaping' ),
                'help'          => '',
                'panels_groups' => array( 'bh_widget_group' ),
                'panels_icon'   => 'dashicons dashicons-edit'
            ),
            array(),
            array(
                'title' => array(
                    'type'                  => 'text',
                    'label'                 => esc_html__( 'Heading Text', 'landscaping' ),
                    'default'               => '',
                    'allow_html_formatting' => array(
                        'span' => array(),
                    )
                ),



                'textcolor' => array(
                    'type'    => 'color',
                    'label'   => esc_html__( 'Text Heading color', 'landscaping' ),
                    'default' => '',
                ),
                'size'      => array(
                    "type"    => "select",
                    "label"   => esc_html__( "Size Heading", 'landscaping' ),
                    "options" => array(
                        "h2" => esc_html__( "h2", 'landscaping' ),
                        "h3" => esc_html__( "h3", 'landscaping' ),
                        "h4" => esc_html__( "h4", 'landscaping' ),
                        "h5" => esc_html__( "h5", 'landscaping' ),
                        "h6" => esc_html__( "h6", 'landscaping' ),
                    ),
                    "default" => "h3"
                ),


                'icon_heading'        => array(
                    "type"          => "select",
                    "label"         => esc_html__( "Icon Heading", 'landscaping' ),
                    "default"       => "default",
                    "options"       => array(
                        "default" => esc_html__( "Default", 'landscaping' ),
                        "custom"  => esc_html__( "Custom", 'landscaping' )
                    ),
                    'state_emitter' => array(
                        'callback' => 'select',
                        'args'     => array( 'icon_heading_type' )
                    )
                ),
                'custom_icon_heading' => array(
                    'type'                  => 'text',
                    'label'                 => esc_html__( 'Custom Icon Heading', 'landscaping' ),
                    'default'               => '',
                    'hide'          => true,
                    'state_handler' => array(
                        'icon_heading_type[custom]'  => array( 'show' ),
                        'icon_heading_type[default]' => array( 'hide' ),
                    ),
                    'allow_html_formatting' => array(
                        'i' => array(),
                    )
                ),
                'view_more'        => array(
                    "type"          => "select",
                    "label"         => esc_html__( "View more", 'landscaping' ),
                    "default"       => "default",
                    "options"       => array(
                        "default" => esc_html__( "(emtpy)", 'landscaping' ),
                        "custom"  => esc_html__( "Custom", 'landscaping' )
                    ),
                    'state_emitter' => array(
                        'callback' => 'select',
                        'args'     => array( 'view_more_type' )
                    )
                ),
                'custom_view_more_label' => array(
                    'type'                  => 'text',
                    'label'                 => esc_html__( 'Custom View More Label', 'landscaping' ),
                    'default'               => '',
                    'hide'          => true,
                    'state_handler' => array(
                        'view_more_type[custom]'  => array( 'show' ),
                        'view_more_type[default]' => array( 'hide' ),
                    ),
                    'allow_html_formatting' => array(
                        'i' => array(),
                    )
                ),
                'custom_view_more_url' => array(
                    'type'                  => 'text',
                    'label'                 => esc_html__( 'Custom View More URL', 'landscaping' ),
                    'default'               => '',
                    'hide'          => true,
                    'state_handler' => array(
                        'view_more_type[custom]'  => array( 'show' ),
                        'view_more_type[default]' => array( 'hide' ),
                    ),
                    'allow_html_formatting' => array(
                        'i' => array(),
                    )
                )
            ),
            BH_DIR  . 'inc/widgets/heading/'
        );
    }

    /**
     * Initialize the CTA widget
     */

    function get_template_name( $instance ) {
        return 'base';
    }

    function get_style_name( $instance ) {
        return false;
    }

}

function bh_heading_register_widget() {
    register_widget( 'BH_Heading_Widget' );
}

add_action( 'widgets_init', 'bh_heading_register_widget' );