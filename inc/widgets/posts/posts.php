<?php

class Rambo_Posts_Widget extends BH_Widget {
	function __construct() {
		parent::__construct(
			'posts',
			esc_html__( 'BH: Posts for Sidebar', 'blackhole' ),
			array(
				'description'   => esc_html__( 'Posts for Sidebar', 'blackhole' ),
				'help'          => '',
				'panels_groups' => array( 'bh_widget_group' ),
				'panels_icon'   => 'dashicons dashicons-format-image'
			),
			array(),
			array(
				'label' => array(
					'type'        => 'text',
					'label'       => esc_html__( 'Label', 'blackhole' ),
					'description' => esc_html__( 'Enter label of section', 'blackhole' )
				),
				'cat_posts' => array(
					"type"    => "select",
					"label"   => esc_html__( "Category", 'blackhole' ),
					"options" => array_merge(array('all' => 'Tất cả'), $this->get_cat_lists()),
				),
				'display' => array(
					"type"    => "select",
					"label"   => esc_html__( "Display", 'blackhole' ),
					"options" => array(
						'normal' => 'Normal',
						'speical' => 'Special'
					),
				),
				'limit' => array(
					'type'        => 'text',
					'default' => '5',
					'label'       => esc_html__( 'Item show', 'blackhole' ),
					'description' => esc_html__( 'Enter number you want show product', 'blackhole' )
				)
			),
			BH_DIR  . 'inc/widgets/posts'
		);
	}

	/**
	 * @param $instance
	 *
	 * @return string
	 */

	function get_template_name( $instance ) {
		return 'base';
	}

	function get_style_name( $instance ) {
		return false;
	}
	function get_cat_lists()
	{
		global $wpdb;
		$terms = $wpdb->get_results( "SELECT * FROM $wpdb->terms LEFT JOIN $wpdb->term_taxonomy ON $wpdb->terms.term_id = $wpdb->term_taxonomy.term_id WHERE taxonomy = 'category'", OBJECT );

		if($terms){
			$option = array();
			foreach ($terms as $key => $term) {

				$option[$term->slug] = esc_html__( $term->name, 'blackhole' );
			}
			return $option;
		}
	}

}

function rambo_posts_widget_register() {
	register_widget( 'Rambo_Posts_Widget' );
}

add_action( 'widgets_init', 'rambo_posts_widget_register' );


