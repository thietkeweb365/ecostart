<?php
global $post, $_product;
$panels_info = $instance['panels_info'];
$label = $instance['label'];
$cat = $instance['cat_posts'];
$display = $instance['display'];
$limit = $instance['limit'];

$tax_query = array();
if($cat && $cat != 'all'){
	$tax_query = array(
	array(
	   'taxonomy' => 'category',
	   'field' => 'slug',
	   'terms' => $cat
	)
  );
}
$query_postargs = array(
  'post_status' => 'publish',
  'post_type' => 'post',
  'posts_per_page' => $limit,
  'tax_query' => $tax_query
);

$query_post = new WP_Query($query_postargs);
	if( $query_post->have_posts() ){?>
        <aside class="widgets">
            <h3 class="widget-title no-box"><?php echo $label;?></h3>
            <ul>
                <?php while ($query_post->have_posts()) : $query_post->the_post();?>
                <li class="tie_audio">
                    <div class="post-thumbnail tie-appear">
                        <?php thim_feature_image( 95, 65, 'full' ); ?>
                        <span class="fa overlay-icon"></span>
                    </div>
                    <?php the_title( sprintf( '<h3 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' ); ?>
                    <span class="tie-date"><i class="fa fa-clock-o"></i>
                        <?php
                        printf('%1$s', get_the_time(get_option('date_format')));
                        ?>
                    </span>
                    <span class="post-comments post-comments-widget"><i class="fa fa-comments"></i>
                        <?php comments_popup_link( esc_html__( '0', 'landscaping' ), esc_html__( '1', 'landscaping' ), esc_html__( '%', 'landscaping' ) ); ?>
                    </span>
                </li>
                <?php endwhile;
                wp_reset_postdata();?>
            </ul>
        </aside>
<?php
    }
?>

