<?php

class Rambo_Products_Widget extends BH_Widget {
	function __construct() {
		parent::__construct(
			'products',
			esc_html__( 'BH: Products', 'blackhole' ),
			array(
				'description'   => esc_html__( 'Products', 'blackhole' ),
				'help'          => '',
				'panels_groups' => array( 'bh_widget_group' ),
				'panels_icon'   => 'dashicons dashicons-format-image'
			),
			array(),
			array(
				'cat' => array(
					"type"    => "select",
					"label"   => esc_html__( "Category", 'blackhole' ),
					"options" => $this->get_term_lists(),
				),
				'limit' => array(
					'type'        => 'text',
					'default' => '4',
					'label'       => esc_html__( 'Item show', 'blackhole' ),
					'description' => esc_html__( 'Enter number you want show product', 'blackhole' )
				)
			),
			BH_DIR  . 'inc/widgets/products'
		);
	}

	/**
	 * @param $instance
	 *
	 * @return string
	 */

	function get_template_name( $instance ) {
		return 'base';
	}

	function get_style_name( $instance ) {
		return false;
	}
	function get_term_lists()
	{
		global $wpdb;
		$terms = $wpdb->get_results( "SELECT * FROM $wpdb->terms LEFT JOIN $wpdb->term_taxonomy ON $wpdb->terms.term_id = $wpdb->term_taxonomy.term_id WHERE taxonomy = 'product_cat'", OBJECT );

		if($terms){
			$option = array();
			foreach ($terms as $key => $term) {
				$option[$term->term_id] = esc_html__( $term->name, 'blackhole' );
			}
			return $option;
		}
	}

}

function rambo_products_widget_register() {
	register_widget( 'Rambo_Products_Widget' );
}

add_action( 'widgets_init', 'rambo_products_widget_register' );


