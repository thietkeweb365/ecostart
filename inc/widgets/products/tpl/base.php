<?php
global $post, $_product;
$panels_info = $instance['panels_info'];
$label = $instance['label'];
$cat = $instance['cat'];
$limit = $instance['limit'];

$cat = get_term_by('term_id', $cat, 'product_cat');


$args = array(
  'post_status' => 'publish',
  'post_type' => 'product',
  'posts_per_page' => $limit,
  'tax_query' => array(
	array(
	   'taxonomy' => 'product_cat',
	   'field' => 'ID',
	   'terms' => $cat->term_id
	)
  )
);

$query = new WP_Query($args);
	if( $query->have_posts() ){?>

		<ul class="product-list">
			<?php while ($query->have_posts()) : $query->the_post();
                wc_get_template_part( 'content', 'product' );
			endwhile;?>
		</ul>
		<!--.product-list-->

<?php }
wp_reset_postdata();?>
</div>
