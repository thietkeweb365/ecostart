<?php

class Rambo_Menu_Footer_Widget extends BH_Widget {
	function __construct() {
		parent::__construct(
			'menu-footer',
			esc_html__( 'Rambo: Menu Footer', 'blackhole' ),
			array(
				'description'   => esc_html__( 'Menu', 'blackhole' ),
				'help'          => '',
				'panels_groups' => array( 'bh_widget_group' ),
				'panels_icon'   => 'dashicons dashicons-format-image'
			),
			array(),
			array(
				'title' => array(
					'type'                  => 'text',
					'label'                 => esc_html__( 'Heading Text', 'blackhole' ),
					'default'               => '',
				),
				'menu_select' => array(
					"type"    => "select",
					"label"   => esc_html__( "Menu", 'blackhole' ),
					"options" => $this->get_menu_select(),
				)
			),
			BH_DIR  . 'inc/widgets/menu-footer'
		);
	}

	/**
	 * @param $instance
	 *
	 * @return string
	 */

	function get_template_name( $instance ) {
		return 'base';
	}

	function get_style_name( $instance ) {
		return false;
	}

	function get_menu_select()
	{
		$menu = get_terms( 'nav_menu', array( 'hide_empty' => true ) );
		if($menu){
			$option = array();
			foreach ($menu as $key => $value) {
				$option[$value->term_id] = esc_html__( $value->name, 'blackhole' );
			}
			return $option;
		}
	}
}

function rambo_menu_footer_widget_register() {
	register_widget( 'Rambo_Menu_Footer_Widget' );
}

add_action( 'widgets_init', 'rambo_menu_footer_widget_register' );


