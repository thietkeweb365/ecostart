<?php
$footer = $titan->createThimCustomizerSection( array(
	'name'     => esc_html__( 'SEO', 'landscaping' ),
	'position' => 3,
	'id'       => 'display_seo'
) );

$footer->createOption( array(
	'name'    => esc_html__( 'H1 (dành cho trang chủ)', 'landscaping' ),
	'id'      => 'seo_h1',
	'type'    => 'text',
	'des'     => esc_html__( 'show or hide h1 tag at homepage', 'landscaping' ),
	'default' => '',
) );

$footer->createOption( array(
	'name'    => esc_html__( 'Google Analytics', 'landscaping' ),
	'id'      => 'seo_analytics',
	'type'    => 'textarea',
	'des'     => esc_html__( 'Emebd code Google Analytics here', 'landscaping' ),
	'default' => '',
) );
$footer->createOption( array(
	'name'    => esc_html__( 'Robots.txt', 'landscaping' ),
	'id'      => 'seo_robots',
	'type'    => 'textarea',
	'des'     => esc_html__( 'Emebd code robots.txt here', 'landscaping' ),
	'default' => '',
) );

add_action( 'wp_head', 'embed_google_analytics' );
function embed_google_analytics(){
	$theme_options_data = get_theme_mods();
	if ( is_admin() || is_feed() || is_robots() || is_trackback() ) {
		return;
	}
	echo wp_unslash($theme_options_data['thim_seo_analytics']);

}


function save_robots(){
	if ( is_admin() ){
		$theme_options_data = get_theme_mods();
		$myFile = str_replace('wp-admin', '', getcwd()).'robots.txt';
		$message = $theme_options_data['thim_seo_robots'];
		if (file_exists($myFile)) {
		  $fh = fopen($myFile, 'w');
		  fwrite($fh, $message."\n");
		} else {
		  $fh = fopen($myFile, 'w');
		  fwrite($fh, $message."\n");
		}
		fclose($fh);
	}

}
add_action( 'customize_save_after', 'save_robots' );

    function log_it( $message ) {
        if( WP_DEBUG === true ){
            if( is_array( $message ) || is_object( $message ) ){
                error_log( print_r( $message, true ) );
            } else {
                error_log( $message );
            }
        }
    }