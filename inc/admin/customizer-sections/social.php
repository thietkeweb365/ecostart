<?php
$footer = $titan->createThimCustomizerSection( array(
	'name'     => esc_html__( 'Mạng xã hội', 'landscaping' ),
	'position' => 3,
	'id'       => 'display_social'
) );

$footer->createOption( array(
	'name'    => esc_html__( 'Facebook', 'landscaping' ),
	'id'      => 'social_facebook',
	'type'    => 'text',
	'des'     => esc_html__( 'show or hide back to top', 'landscaping' ),
	'default' => '',
) );

$footer->createOption( array(
	'name'        => esc_html__( 'Google+', 'landscaping' ),
	'id'          => 'social_google',
	'type'        => 'text',
	'default'     => ''
) );
$footer->createOption( array(
	'name'        => esc_html__( 'Twitter', 'landscaping' ),
	'id'          => 'social_twitter',
	'type'        => 'text',
	'default'     => ''
) );
$footer->createOption( array(
	'name'        => esc_html__( 'Youtube', 'landscaping' ),
	'id'          => 'social_youtube',
	'type'        => 'text',
	'default'     => ''
) );


$footer->createOption( array(
	'name'        => esc_html__( 'Skype', 'landscaping' ),
	'id'          => 'social_skype',
	'type'        => 'text',
	'default'     => ''
) );
$footer->createOption( array(
	'name'        => esc_html__( 'Viber', 'landscaping' ),
	'id'          => 'social_viber',
	'type'        => 'text',
	'default'     => ''
) );
$footer->createOption( array(
	'name'        => esc_html__( 'Zalo', 'landscaping' ),
	'id'          => 'social_zalo',
	'type'        => 'text',
	'default'     => ''
) );
