<?php
$font_sizes = array(
    '10px' => '10px',
    '11px' => '11px',
    '12px' => '12px',
    '13px' => '13px',
    '14px' => '14px',
    '15px' => '15px',
    '16px' => '16px',
    '17px' => '17px',
    '18px' => '18px',
    '19px' => '19px',
    '20px' => '20px',
    '21px' => '21px',
    '22px' => '22px',
    '23px' => '23px',
    '24px' => '24px',
    '25px' => '25px',
    '26px' => '26px',
    '27px' => '27px',
    '28px' => '28px',
    '29px' => '29px',
    '30px' => '30px',
    '31px' => '31px',
    '32px' => '32px',
    '33px' => '33px',
    '34px' => '34px',
    '35px' => '35px',
    '36px' => '36px',
    '37px' => '37px',
    '38px' => '38px',
    '39px' => '39px',
    '40px' => '40px',
    '41px' => '41px',
    '42px' => '42px',
    '43px' => '43px',
    '44px' => '44px',
    '45px' => '45px',
    '46px' => '46px',
    '47px' => '47px',
    '48px' => '48px',
    '49px' => '49px',
    '50px' => '50px',
);
$woo = $titan->createThimCustomizerSection( array(
	'name'     => esc_html__( 'WooCommerce', 'landscaping' ),
	'position' => 3,
	'id'       => 'display_woo'
) );

$woo->createOption( array(
    "name"    => esc_html__( "Kiểu hiển thị", 'landscaping' ),
    'desc'    => esc_html__( 'Chọn kiểu hiển thị cho sản phẩm', 'landscaping' ),
    "id"      => "woo_type",
    "default" => "default",
    "type"    => "select",
    "options" => array(
        'default'     => 'Default',
        'special'      => 'Special'
    ),
) );
$woo->createOption( array(
    'name'    => esc_html__( 'Độ dài mô tả sản phẩm', 'landscaping' ),
    'id'      => 'woo_excerpt_length',
    'type'    => 'number',
    'des'     => esc_html__( 'show or hide h1 tag at homepage', 'landscaping' ),
    'default' => 120,
    'max' => 160
) );
$woo->createOption( array(
    'name'    => esc_html__( 'Nhãn liên hệ', 'landscaping' ),
    'id'      => 'woo_price_contact',
    'type'    => 'text',
    'des'     => esc_html__( 'show or hide h1 tag at homepage', 'landscaping' ),
    'default' => 'Liên hệ',
    'max' => 160
) );
$woo->createOption( array(
    'name'    => esc_html__( 'Đơn vị tiền', 'landscaping' ),
    'id'      => 'woo_price_contact_currency',
    'type'    => 'text',
    'des'     => esc_html__( 'show or hide h1 tag at homepage', 'landscaping' ),
    'default' => '',
    'max' => 160
) );

$woo->createOption( array(
    'name'        => esc_html__( 'Màu sắc giá', 'landscaping' ),
    'id'          => 'woo_price_color',
    'type'        => 'color-opacity',
    'default'     => '#444',
) );

$woo->createOption( array(
    "name"    => esc_html__( "Kích thước giá", 'landscaping' ),
    "desc"    => esc_html__( "Mặc định là 14", 'landscaping' ),
    "id"      => "woo_price_size",
    "default" => "14px",
    "type"    => "select",
    "options" => $font_sizes
) );
$woo->createOption( array(
    "name"    => esc_html__( "Kích thước tên sản phẩm", 'landscaping' ),
    "desc"    => esc_html__( "Mặc định là 16", 'landscaping' ),
    "id"      => "woo_title_product_size",
    "default" => "16px",
    "type"    => "select",
    "options" => $font_sizes
) );

add_filter('woocommerce_currency_symbol', 'add_my_currency_symbol', 10, 2);

function add_my_currency_symbol( $currency_symbol, $currency ) {
    $theme_options_data = get_theme_mods();
    if($theme_options_data['thim_woo_price_contact_currency']){
        $currency_symbol = $theme_options_data['thim_woo_price_contact_currency'];
    }
    
    return $currency_symbol;
}