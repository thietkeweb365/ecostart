<?php
/*
 * Creating a logo Options
 */
$logo = $titan->createThemeCustomizerSection( array(
	'name'     => 'title_tagline',
	'position' => 1,
) );

$logo->createOption( array(
	'name'    => esc_html__( 'Logo', 'landscaping' ),
	'id'      => 'logo',
	'type'    => 'upload',
	'desc'    => esc_html__( 'Upload your logo', 'landscaping' ),
	'default' => get_template_directory_uri( 'template_directory' ) . "/images/logo.png",
) );


?>