<?php

$color = $titan->createThimCustomizerSection( array(
	'name'     => esc_html__( 'Màu sắc', 'landscaping' ),
	'position' => 4,
	'id'       => 'display_color'
) );



$color->createOption( array(
    'name'        => esc_html__( 'Màu sắc văn bản', 'landscaping' ),
    'id'          => 'color_text',
    'type'        => 'color-opacity',
    'default'     => '#777',
) );
$color->createOption( array(
    'name'        => esc_html__( 'Màu sắc tiêu đề', 'landscaping' ),
    'id'          => 'color_heading',
    'type'        => 'color-opacity',
    'default'     => '#313131',
) );


add_action('wp_head', 'my_custom_styles', 100);

function my_custom_styles()
{
    $theme_options_data = get_theme_mods();
 echo "<style>body{color: " . $theme_options_data['thim_color_text']."}.widget h3.widget-title, .widget h3.widget-title.no-box{color: " . $theme_options_data['thim_color_heading']."}</style>";
}