<?php
$footer = $titan->createThimCustomizerSection( array(
	'name'     => esc_html__( 'Thông tin liên hệ', 'landscaping' ),
	'position' => 3,
	'id'       => 'display_infor'
) );
$footer->addSubSection( array(
	'name'     => esc_html__( 'Thông tin', 'landscaping' ),
	'id'       => 'info_subs',
	'position' => 1,
) );


$footer->createOption( array(
	'name'    => esc_html__( 'Địa chỉ', 'landscaping' ),
	'id'      => 'infor_address',
	'type'    => 'text',
	'des'     => esc_html__( 'show or hide back to top', 'landscaping' ),
	'default' => '',
) );


$footer->createOption( array(
	'name'    => esc_html__( 'Số điện thoại', 'landscaping' ),
	'id'      => 'infor_tel',
	'type'    => 'text',
	'des'     => esc_html__( 'show or hide back to top', 'landscaping' ),
	'default' => '',
) );

$footer->createOption( array(
	'name'    => esc_html__( 'Nhãn phía dưới số điện thoại', 'landscaping' ),
	'id'      => 'infor_tel_desc',
	'type'    => 'text',
	'des'     => esc_html__( 'show or hide back to top', 'landscaping' ),
	'default' => '',
) );


$footer->createOption( array(
	'name'    => esc_html__( 'Email', 'landscaping' ),
	'id'      => 'infor_email',
	'type'    => 'text',
	'des'     => esc_html__( 'show or hide back to top', 'landscaping' ),
	'default' => '',
) );

$footer->createOption( array(
	'name'    => esc_html__( 'Nhãn phía dưới địa chỉ email', 'landscaping' ),
	'id'      => 'infor_email_desc',
	'type'    => 'text',
	'des'     => esc_html__( 'show or hide back to top', 'landscaping' ),
	'default' => '',
) );
$footer->addSubSection( array(
	'name'     => esc_html__( 'Call me', 'landscaping' ),
	'id'       => 'info_subs_callme',
	'position' => 2,
) );


$footer->createOption( array(
	'name'    => esc_html__( 'Ảnh', 'landscaping' ),
	'id'      => 'infor_callme',
	'type'    => 'upload',
	'desc'    => esc_html__( 'Upload your logo', 'landscaping' ),
	'default' => get_template_directory_uri( 'template_directory' ) . "/images/logo.png",
) );

$footer->createOption( array(
    "name"    => esc_html__( "Kiểu hiển thị", 'landscaping' ),
    'desc'    => esc_html__( 'Chọn kiểu hiển thị cho ảnh', 'landscaping' ),
    "id"      => "info_callme_show",
    "default" => "right",
    "type"    => "select",
    "options" => array(
        'right'     => 'Phải',
        'left'      => 'Trái'
    ),
) );
$footer->createOption( array(
	'name'    => esc_html__( 'Số điện thoại', 'landscaping' ),
	'id'      => 'infor_callme_tel',
	'type'    => 'text',
	'des'     => esc_html__( 'show or hide back to top', 'landscaping' ),
	'default' => '02388980678',
) );