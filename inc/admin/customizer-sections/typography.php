<?php

$typography = $titan->createThimCustomizerSection( array(
	'name'     => esc_html__( 'Typography', 'landscaping' ),
	'position' => 7,
	'id'       => 'typography'
) );

$typography->addSubSection( array(
	'name'     => esc_html__( 'Body', 'landscaping' ),
	'id'       => 'typography_font_body',
	'position' => 1,

) );
$typography->createOption( array(
	'name'                => esc_html__( 'Select Font', 'landscaping' ),
	'id'                  => 'font_bodys',
	'type'                => 'font-color',
	'show_font_family'    => false,
	'show_font_weight'    => false,
	'show_font_style'     => false,
	'show_font_size'      => false,
	'show_line_height'    => false,
	'show_letter_spacing' => false,
	'show_text_transform' => false,
	'show_font_variant'   => false,
	'show_text_shadow'    => false,
	'default'             => false
) );
