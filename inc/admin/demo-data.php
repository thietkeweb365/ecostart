<?php

defined( 'DS' ) OR define( 'DS', DIRECTORY_SEPARATOR );

$demo_datas_dir = BH_DIR  . 'inc' . DIRECTORY_SEPARATOR . 'admin' . DIRECTORY_SEPARATOR . 'data';

$demo_datas = array(

	'demo-01' => array(
		'data_dir'      => $demo_datas_dir . DS . 'demo-01',
		'thumbnail_url' => THIM_URI . 'inc/admin/data/demo-01/thumb.jpg',
		'title'         => esc_html__( 'Demo 01', 'landscaping' ),
		'demo_url'      => 'http://landscaping.thimpress.com/',
	),

	'demo-02' => array(
		'data_dir'      => $demo_datas_dir . DS . 'demo-02',
		'thumbnail_url' => THIM_URI . 'inc/admin/data/demo-02/thumb.jpg',
		'title'         => esc_html__( 'Demo 02', 'landscaping' ),
		'demo_url'      => 'http://landscaping.thimpress.com/demo-2/',
	),

	'demo-03' => array(
		'data_dir'      => $demo_datas_dir . DS . 'demo-03',
		'thumbnail_url' => THIM_URI . 'inc/admin/data/demo-03/thumb.jpg',
		'title'         => esc_html__( 'Demo 03', 'landscaping' ),
		'demo_url'      => 'http://landscaping.thimpress.com/demo-3/',
	),
);