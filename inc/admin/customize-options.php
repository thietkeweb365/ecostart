<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of customizer-options
 *
 * @author Tuannv
 */


class Thim_Customize_Options {

	function __construct() {
		add_action( 'tf_create_options', array( $this, 'thim_create_customizer_options' ) );
		add_action( 'customize_save_after', array( $this, 'thim_generate_to_css' ) );

		/* Unregister Default Customizer Section */
		add_action( 'customize_register', array( $this, 'thim_unregister' ) );
	}

	function thim_unregister( $wp_customize ) {

		$wp_customize->remove_section( 'colors' );
		$wp_customize->remove_section( 'background_image' );
		$wp_customize->remove_section( 'nav' );

	}

	function thim_create_customizer_options() {
		$titan                                       = TitanFramework::getInstance( 'thim' );

		/* Register Customizer Sections */
		//include heading
		include BH_DIR  . '/inc/admin/customizer-sections/logo.php';





		//include typography
		include BH_DIR  . '/inc/admin/customizer-sections/typography.php';
		include BH_DIR  . '/inc/admin/customizer-sections/color.php';
		//include footer
		include BH_DIR  . '/inc/admin/customizer-sections/social.php';
		//include footer
		include BH_DIR  . '/inc/admin/customizer-sections/info.php';
		//include seo
		include BH_DIR  . '/inc/admin/customizer-sections/seo.php';
        //include woo
        include BH_DIR  . '/inc/admin/customizer-sections/woocommerce.php';
		//include Custom Css
		include BH_DIR  . '/inc/admin/customizer-sections/custom-css.php';


	}


}

new Thim_Customize_Options();