<?php

$comingsoon = $titan->createMetaBox( array(
	'name'      => esc_html__( 'Coming Soon Options', 'landscaping' ),
	'id'        => 'coming-soon-mode-options',
	'post_type' => array( 'page' ),
) );

$comingsoon->createOption( array(
	'name' => esc_html__( 'Background', 'landscaping' ),
	'id'   => 'comingsoon_image',
	'type' => 'upload',
	'desc' => esc_html__( 'Upload your image', 'landscaping' ),
	'default' => get_template_directory_uri( 'template_directory' ) . "/images/coming-soon.jpg"
) );


$comingsoon->createOption( array(
	'name'    => esc_html__( 'Date', 'landscaping'),
	'id'      => 'comingsoon_date',
	'type'    => 'date',
	'desc'    => esc_html__( 'Choose a date', 'landscaping' ),
	'time'		=> true,
	'default' => '',
) );

$comingsoon->createOption( array(
	'name'    => esc_html__( 'Title','landscaping'),
	'id'      => 'comingsoon_title',
	'type'    => 'text',
	'default' => esc_html__("We're Coming Soon", 'landscaping'),
) );


$comingsoon->createOption( array(
	'name'    => esc_html__( 'Description','landscaping'),
	'id'      => 'comingsoon_description',
	'type'    => 'text',
	'default' => esc_html__("Leave your email and we'll let you know once the site goes live. Plus your get a free gift!", 'landscaping'),
) );

$comingsoon->createOption( array(
	'name' => esc_html__( 'ShortCode','landscaping'),
	'id'   => 'comingsoon_shortcode',
	'type' => 'text',
	'default' => '[mc4wp_form]',
) );


