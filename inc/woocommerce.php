<?php
class BH_WooCommerce {
	public $theme_options_data;
    /**
     * Constructor.
     */
    public function __construct() {

        remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10,0);
        remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20,0);
        remove_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10,0);
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10, 0);
        remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10, 2 );
        remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10, 2 );
        remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20, 0);
        remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30, 0);





        add_action('woocommerce_before_main_content', array($this, 'bh_woocommerce_breadcrumb'), 10,0);
        add_action('woocommerce_before_main_content', array($this, 'bh_woocommerce_output_content_wrapper'), 20,0);
        add_action('woocommerce_after_main_content', array($this, 'bh_woocommerce_output_content_wrapper_end'), 10,0);
        add_action('woocommerce_single_product_summary', array($this, 'bh_woocommerce_template_single_price'), 25,0);
        $this->theme_options_data = get_theme_mods();
        if($this->theme_options_data['thim_woo_type'] == 'default'){
            add_filter('woocommerce_product_get_rating_html', array($this, 'bh_woocommerce_product_get_rating_html'), 10,2);
        }else{
            add_filter('woocommerce_product_get_rating_html', array($this, 'remove_woocommerce_product_get_rating_html'), 10,2);
            add_filter('woocommerce_template_loop_add_to_cart', array($this, 'bh_woocommerce_template_loop_add_to_cart'), 10,2);
            add_filter( 'woocommerce_loop_add_to_cart_link', array($this, 'filter_woocommerce_loop_add_to_cart_link'), 10, 2 );

            remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );

        }
        add_action('woocommerce_before_shop_loop_item_title', array($this, 'bh_woocommerce_template_loop_product_thumbnail'), 10, 3);
        add_action('woocommerce_share', array($this, 'bh_woocommerce_share'), 10, 0);

        require BH_DIR  . 'inc/class.woocommerce-advanced-reviews.php';

        add_action( 'wp_footer', array($this, 'footer_call_me') );
    }


    function footer_call_me(){
        if($this->theme_options_data['thim_infor_callme']){
            $img_src = wp_get_attachment_image_src($this->theme_options_data['thim_infor_callme'], 'full');
            echo '<div class="call-me '.$this->theme_options_data['thim_info_callme_show'].'"><a href="tel:'.$this->theme_options_data['thim_infor_callme_tel'].'"><img src="'.$img_src[0].'"></a></div>';
        }
        

    }

    function filter_woocommerce_loop_add_to_cart_link( $html, $product ) {
        return '<p class="product-desc">'.get_excerpt( get_the_excerpt($product->id), $this->theme_options_data['thim_woo_excerpt_length'], '...').'</p>';
    }


    /**
     * Breadcrumb
     **/
    function bh_woocommerce_breadcrumb($args = array()){
        $args = wp_parse_args( $args, apply_filters( 'woocommerce_breadcrumb_defaults', array(
            'delimiter'   => '&nbsp;&#47;&nbsp;',
            'wrap_before' => '<div class="breadcrumbs-wrap"><div class="container"><ol ' . ( is_single() ? 'itemscope="" itemtype="http://schema.org/BreadcrumbList"' : '' ) . '>',
            'wrap_after'  => '</ol></div></div>',
            'before'      => '',
            'after'       => '',
            'home'        => _x( 'Home', 'breadcrumb', 'woocommerce' )
        ) ) );

        $breadcrumbs = new WC_Breadcrumb();

        if ( ! empty( $args['home'] ) ) {
            $breadcrumbs->add_crumb( $args['home'], apply_filters( 'woocommerce_breadcrumb_home_url', home_url() ) );
        }

        $args['breadcrumb'] = $breadcrumbs->generate();

        wc_get_template( 'global/breadcrumb.php', $args );
    }

    /**
     * @hooked: woocommerce_output_content_wrapper
     **/
    function bh_woocommerce_output_content_wrapper(){
        wc_get_template( 'global/wrapper-start.php' );
    }
    /**
     * Output the end of the page wrapper.
     *
     */
    function bh_woocommerce_output_content_wrapper_end() {
        wc_get_template( 'global/wrapper-end.php' );
    }

    /**
     * Output the product price.
     *
     * @subpackage	Product
     */
    function bh_woocommerce_template_single_price() {
        wc_get_template( 'single-product/price.php' );
    }

    function bh_woocommerce_product_get_rating_html($rating_html, $rating){

        $rating_html  = '<div class="rating-wrap" title="' . sprintf( __( 'Rated %s out of 5', 'woocommerce' ), $rating ) . '"><span class="rating-before"><span class="rating-line"></span></span>';
        $rating_html .= '<div class="rating-content"><div class="star-rating" title="" data-original-title="'. $rating.'"><span style="width:' . ( ( $rating / 5 ) * 100 ) . '%"><strong class="rating">' . $rating . '</strong> ' . __( 'out of 5', 'woocommerce' ) . '</span></div></div>';
        $rating_html .= '<span class="rating-after"><span class="rating-line"></span></span></div>';

        return $rating_html;
    }

    function remove_woocommerce_product_get_rating_html($rating_html, $rating){
        return '';
    }

    /**
     * Get the product thumbnail for the loop.
     *
     * @subpackage	Loop
     */
    function bh_woocommerce_template_loop_product_thumbnail($size = 'shop_catalog', $deprecated1 = 0, $deprecated2 = 0) {
        global $post;
        $image_size = apply_filters( 'single_product_archive_thumbnail_size', $size );
        echo '<a href="'.get_permalink($post->ID).'"><div class="product-image">';
        echo woocommerce_show_product_loop_sale_flash();
        if ( has_post_thumbnail() ) {
            $props = wc_get_product_attachment_props( get_post_thumbnail_id(), $post );
            echo get_the_post_thumbnail( $post->ID, $image_size, array(
                'title'	 => $props['title'],
                'alt'    => $props['alt'],
            ) );
        } elseif ( wc_placeholder_img_src() ) {
            echo wc_placeholder_img( $image_size );
        }
        echo '</div></a>';
    }

    function bh_woocommerce_share(){
        global $post;
        echo '<div class="share-links"><a href="http://www.facebook.com/sharer.php?m2w&amp;s=100&amp;p[url]='. esc_url( get_permalink($post-> ID) ) . '&amp;p[images][0]=http://www.newsmartwave.net/wordpress/porto/shop1/wp-content/uploads/sites/20/2016/06/product_07.jpg&amp;p[title]=Ladies Handbag" target="_blank" rel="nofollow" data-tooltip="" data-placement="bottom" title="" class="share-facebook" data-original-title="Facebook">Facebook</a><a href="https://twitter.com/intent/tweet?text=Ladies Handbag&amp;url='. esc_url( get_permalink($post-> ID) ) . '" target="_blank" rel="nofollow" data-tooltip="" data-placement="bottom" title="" class="share-twitter" data-original-title="Twitter">Twitter</a><a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=http://www.newsmartwave.net/wordpress/porto/shop1/product/ladies-handbag-2/&amp;title=Ladies Handbag" target="_blank" rel="nofollow" data-tooltip="" data-placement="bottom" title="" class="share-linkedin" data-original-title="LinkedIn">LinkedIn</a><a href="https://plus.google.com/share?url=http://www.newsmartwave.net/wordpress/porto/shop1/product/ladies-handbag-2/" target="_blank" rel="nofollow" data-tooltip="" data-placement="bottom" title="" class="share-googleplus" data-original-title="Google +">Google +</a><a href="mailto:?subject=Ladies Handbag&amp;body=http://www.newsmartwave.net/wordpress/porto/shop1/product/ladies-handbag-2/" target="_blank" rel="nofollow" data-tooltip="" data-placement="bottom" title="" class="share-email" data-original-title="Email">Email</a></div>';
    }


}
new BH_WooCommerce();

