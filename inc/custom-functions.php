<?php
function thim_hex2rgb( $hex ) {
	$hex = str_replace( "#", "", $hex );
	if ( strlen( $hex ) == 3 ) {
		$r = hexdec( substr( $hex, 0, 1 ) . substr( $hex, 0, 1 ) );
		$g = hexdec( substr( $hex, 1, 1 ) . substr( $hex, 1, 1 ) );
		$b = hexdec( substr( $hex, 2, 1 ) . substr( $hex, 2, 1 ) );
	} else {
		$r = hexdec( substr( $hex, 0, 2 ) );
		$g = hexdec( substr( $hex, 2, 2 ) );
		$b = hexdec( substr( $hex, 4, 2 ) );
	}
	$rgb = array( $r, $g, $b );

	return $rgb; // returns an array with the rgb values
}

function thim_getExtraClass( $el_class ) {
	$output = '';
	if ( $el_class != '' ) {
		$output = " " . str_replace( ".", "", $el_class );
	}

	return $output;
}

function thim_getCSSAnimation( $css_animation ) {
	$output = '';
	if ( $css_animation != '' ) {
		$output = ' wpb_animate_when_almost_visible wpb_' . $css_animation;
	}

	return $output;
}

function thim_excerpt( $limit ) {
	$content = get_the_excerpt();
	$content = apply_filters( 'the_content', $content );
	$content = str_replace( ']]>', ']]&gt;', $content );
	$content = explode( ' ', $content, $limit );
	array_pop( $content );
	$content = implode( " ", $content );

	return $content;
}

/************ List Comment ***************/
if ( ! function_exists( 'thim_comment' ) ) {
	function thim_comment( $comment, $args, $depth ) {
		$GLOBALS['comment'] = $comment;
		//extract( $args, EXTR_SKIP );
		if ( 'div' == $args['style'] ) {
			$tag       = 'div';
			$add_below = 'comment';
		} else {
			$tag       = 'li';
			$add_below = 'div-comment';
		}
		?>
		<<?php echo ent2ncr( $tag ) . ' '; ?><?php comment_class( 'description_comment' ) ?> id="comment-<?php comment_ID() ?>">
		<div class="wrapper-comment">
			<?php
			if ( $args['avatar_size'] != 0 ) {
				echo get_avatar( $comment, $args['avatar_size'] );
			}
			?>
			<div class="comment-right">
				<?php if ( $comment->comment_approved == '0' ) : ?>
					<em class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'landscaping' ) ?></em>
				<?php endif; ?>

				<div class="comment-extra-info">
					<div
						class="author"><?php printf( '<span class="author-name">%s</span>', get_comment_author_link() ) ?></div>
					<div class="date">
						<?php printf( get_comment_date(), get_comment_time() ) ?></div>
					<?php edit_comment_link( esc_html__( 'Edit', 'landscaping' ), '', '' ); ?>

					<?php comment_reply_link( array_merge( $args, array(
						'add_below' => $add_below,
						'depth'     => $depth,
						'max_depth' => $args['max_depth']
					) ) ) ?>
				</div>

				<div class="content-comment">
					<?php comment_text() ?>
				</div>
			</div>
		</div>
		<?php
	}
}
/************end list comment************/

function thim_move_comment_field_to_bottom( $fields ) {
	$comment_field = $fields['comment'];
	unset( $fields['comment'] );
	$fields['comment'] = $comment_field;

	return $fields;
}

add_filter( 'comment_form_fields', 'thim_move_comment_field_to_bottom' );


/********************************************************************
 * Get image attach
 ********************************************************************/
function thim_feature_image( $width = 1024, $height = 768, $link = true ) {
	global $post;
	if ( has_post_thumbnail() ) {
		if ( $link != true && $link != false ) {
			the_post_thumbnail( $post->ID, $link );
		} else {
			$get_thumbnail = simplexml_load_string( get_the_post_thumbnail( $post->ID, 'full' ) );
			if ( $get_thumbnail ) {
				$thumbnail_src = $get_thumbnail->attributes()->src;
				$img_url       = $thumbnail_src;
				if ( $link ) {
					$image_crop = aq_resize( $img_url[0], $width, $height, true );
					echo '<div class="thumbnail"><a href="' . esc_url( get_permalink() ) . '" title = "' . get_the_title() . '">';
					echo '<img src="' . $image_crop . '" alt= "' . get_the_title() . '" title = "' . get_the_title() . '" />';
					echo '</a></div>';
					// }
				} else {
					$image_crop = aq_resize( $img_url[0], $width, $height, true );

					return '<img src="' . $image_crop . '" alt= "' . get_the_title() . '" title = "' . get_the_title() . '" />';
				}
			}
		}
	}
}

#remove field in Display settings
require BH_DIR  . 'inc/wrapper-before-after.php';

add_filter( 'thim_mtb_setting_after_created', 'thim_mtb_setting_after_created', 10, 2 );
function thim_mtb_setting_after_created( $mtb_setting ) {
	// $mtb_setting->removeOption( array( 1,6, 9, 10, 11 ) );
	// $mtb_setting->removeOption( array( 9, 10, 11) );

	$settings = array(
		'name' => esc_html__( 'No Padding Content', 'landscaping' ),
		'id'   => 'mtb_no_padding',
		'type' => 'checkbox',
		'desc' => ' ',
	);

	$mtb_setting->insertOptionBefore( $settings, 15 );

	return $mtb_setting;
}

//thim_excerpt_length
function thim_excerpt_length() {
	$theme_options_data = get_theme_mods();
	if ( isset( $theme_options_data['thim_archive_excerpt_length'] ) ) {
		$length = $theme_options_data['thim_archive_excerpt_length'];
	} else {
		$length = '50';
	}

	return $length;
}

add_filter( 'excerpt_length', 'thim_excerpt_length', 999 );
function thim_wp_new_excerpt( $text ) {
	if ( $text == '' ) {
		$text           = get_the_content( '' );
		$text           = strip_shortcodes( $text );
		$text           = apply_filters( 'the_content', $text );
		$text           = str_replace( ']]>', ']]>', $text );
		$text           = strip_tags( $text );
		$text           = nl2br( $text );
		$excerpt_length = apply_filters( 'excerpt_length', 55 );
		$words          = explode( ' ', $text, $excerpt_length + 1 );
		if ( count( $words ) > $excerpt_length ) {
			array_pop( $words );
			array_push( $words, '' );
			$text = implode( ' ', $words );
		}
	}

	return $text;
}

remove_filter( 'get_the_excerpt', 'wp_trim_excerpt' );
add_filter( 'get_the_excerpt', 'thim_wp_new_excerpt' );

function thim_post_share() {
	$theme_options_data = get_theme_mods();
	$post_type          = get_post_type();

	$prefix = 'thim_archive_';
	if ( $post_type === 'tp_event' ) {
		$prefix = 'thim_event_';
	}
	$facebook  = isset( $theme_options_data[ $prefix . 'sharing_facebook' ] ) ? $theme_options_data[ $prefix . 'sharing_facebook' ] : true;
	$twitter   = isset( $theme_options_data[ $prefix . 'sharing_twitter' ] ) ? $theme_options_data[ $prefix . 'sharing_twitter' ] : true;
	$google    = isset( $theme_options_data[ $prefix . 'sharing_google' ] ) ? $theme_options_data[ $prefix . 'sharing_google' ] : true;
	$pinterest = isset( $theme_options_data[ $prefix . 'sharing_pinterest' ] ) ? $theme_options_data[ $prefix . 'sharing_pinterest' ] : true;
	$linkedin  = isset( $theme_options_data[ $prefix . 'sharing_linkedin' ] ) ? $theme_options_data[ $prefix . 'sharing_linkedin' ] : true;

	if ( $facebook || $twitter || $pinterest || $linkedin || $google || $linkedin ) {
		echo '<div class="share-links">';
		if ( $facebook == 1 ) {
			echo '<a target="_blank" rel="nofollow" data-tooltip="" data-placement="bottom" title="" class="share-facebook" data-original-title="' . esc_attr__( 'Facebook', 'landscaping' ) . '" href="https://www.facebook.com/sharer.php?u=' . urlencode( get_permalink() ) . '" title="' . esc_attr__( 'Facebook', 'landscaping' ) . '"><i class="fa fa-facebook"></i></a>';
		}
		if ( $twitter == 1 ) {
			echo '<a target="_blank" rel="nofollow" data-tooltip="" data-placement="bottom" title="" class="share-twitter" data-original-title="' . esc_attr__( 'Twitter', 'landscaping' ) . '" href="https://twitter.com/share?url=' . urlencode( get_permalink() ) . '&amp;text=' . rawurlencode( esc_attr( get_the_title() ) ) . '" title="' . esc_attr__( 'Twitter', 'landscaping' ) . '"><i class="fa fa-twitter"></i></a>';
		}
		if ( $google == 1 ) {
			echo '<a target="_blank" rel="nofollow" data-tooltip="" data-placement="bottom" title="" class="share-googleplus" data-original-title="' . esc_attr__( 'Google Plus', 'landscaping' ) . '" href="https://plus.google.com/share?url=' . urlencode( get_permalink() ) . '&amp;title=' . rawurlencode( esc_attr( get_the_title() ) ) . '" title="' . esc_attr__( 'Google Plus', 'landscaping' ) . '" onclick=\'javascript:window.open(this.href, "", "menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600");return false;\'><i class="fa fa-google-plus"></i></a>';
		}
		if ( $pinterest == 1 ) {
			echo '<a target="_blank" rel="nofollow" data-tooltip="" data-placement="bottom" title="" class="share-pinterest" data-original-title="' . esc_attr__( 'Pinterest', 'landscaping' ) . '"  href="http://pinterest.com/pin/create/button/?url=' . urlencode( get_permalink() ) . '&amp;description=' . rawurlencode( esc_attr( get_the_excerpt() ) ) . '&amp;media=' . urlencode( wp_get_attachment_url( get_post_thumbnail_id() ) ) . '" onclick="window.open(this.href); return false;" title="' . esc_attr__( 'Pinterest', 'landscaping' ) . '"><i class="fa fa-pinterest"></i></a>';
		}
		if ( $linkedin == 1 ) {
			echo '<a target="_blank" rel="nofollow" data-tooltip="" data-placement="bottom" title="" class="share-linkedin" data-original-title="' .  esc_attr__( 'LinkedIn', 'landscaping' ) . '"  href="https://www.linkedin.com/cws/share?url=' . urlencode( get_permalink() ) . '&amp;description=' . rawurlencode( esc_attr( get_the_excerpt() ) ) . '&amp;media=' . urlencode( wp_get_attachment_url( get_post_thumbnail_id() ) ) . '" onclick="window.open(this.href); return false;" title="' . esc_attr__( 'LinkedIn', 'landscaping' ) . '"><i class="fa fa-linkedin"></i></a>';
		}
		echo '</div>';
	}

}

add_action( 'thim_social_share', 'thim_post_share' );


add_filter( 'wp_nav_menu_args', 'thim_select_main_menu' );
function thim_select_main_menu( $args ) {
	global $post;
	if ( $post ) {
		if ( get_post_meta( $post->ID, 'thim_select_menu_one_page', true ) != 'default' && ( $args['theme_location'] == 'primary' ) ) {
			$menu         = get_post_meta( $post->ID, 'thim_select_menu_one_page', true );
			$args['menu'] = $menu;
		}
	}

	return $args;
}

add_filter( 'wpcf7_support_html5_fallback', '__return_true' );


/**
 * About the author
 */
function thim_about_author() {
	if ( ! is_singular( 'post' ) || get_the_author_meta( 'description' ) || get_the_author_meta( 'description' ) == '' ) {
		return;
	}
	?>
	<div class="thim-about-author">
		<div class="author-wrapper">
			<div class="author-avatar">
				<?php echo get_avatar( get_the_author_meta( 'email' ), 100 ); ?>
				<p class="name">
					<?php the_author_link(); ?>

				</p>

				<?php if ( get_the_author_meta( 'job' ) ) : ?>
					<p class="job">
						<?php echo get_the_author_meta( 'job' ); ?>
					</p>
				<?php endif; ?>
			</div>
			<div class="author-details">
				<div class="author-bio">


					<p class="description"><?php echo get_the_author_meta( 'description' ); ?></p>

					<?php if ( get_the_author_meta( 'user_url' ) || get_the_author_meta( 'facebook' ) || get_the_author_meta( 'twitter' ) || get_the_author_meta( 'skype' ) || get_the_author_meta( 'pinterest' ) ): ?>
						<ul class="user-social">

							<?php if ( get_the_author_meta( 'user_url' ) ) : ?>
								<li class="user_url">
									<a href="<?php echo esc_url( get_the_author_meta( 'user_url' ) ); ?>"
									   target="_blank"><i
											class="fa fa-globe"></i></a>
								</li>
							<?php endif; ?>

							<?php if ( get_the_author_meta( 'facebook' ) ) : ?>
								<li class="facebook">
									<a href="<?php echo esc_url( get_the_author_meta( 'facebook' ) ); ?>"
									   target="_blank"><i
											class="fa fa-facebook"></i></a>
								</li>
							<?php endif; ?>

							<?php if ( get_the_author_meta( 'twitter' ) ) : ?>
								<li class="twitter">
									<a href="<?php echo esc_url( get_the_author_meta( 'twitter' ) ); ?>"
									   target="_blank"><i
											class="fa fa-twitter"></i></a>
								</li>
							<?php endif; ?>

							<?php if ( get_the_author_meta( 'skype' ) ) : ?>
								<li class="skype">
									<a href="<?php echo esc_url( get_the_author_meta( 'skype' ) ); ?>"
									   target="_blank"><i
											class="fa fa-skype"></i></a>
								</li>
							<?php endif; ?>

							<?php if ( get_the_author_meta( 'pinterest' ) ) : ?>
								<li class="pinterest">
									<a href="<?php echo esc_url( get_the_author_meta( 'pinterest' ) ); ?>"
									   target="_blank"><i
											class="fa fa-pinterest"></i></a>
								</li>
							<?php endif; ?>

						</ul>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<?php
}

add_action( 'thim_about_author', 'thim_about_author' );


function thim_modify_contact_methods( $profile_fields ) {
	$profile_fields['job']       = 'Job';
	$profile_fields['facebook']  = 'Facebook';
	$profile_fields['twitter']   = 'Twitter';
	$profile_fields['skype']     = 'Skype';
	$profile_fields['pinterest'] = 'Pinterest';

	return $profile_fields;
}

add_filter( 'user_contactmethods', 'thim_modify_contact_methods' );


/**
 * Optimize script files
 */
function thim_optimize_scripts() {
	global $wp_scripts;
	if ( ! is_a( $wp_scripts, 'WP_Scripts' ) ) {
		return;
	}
	foreach ( $wp_scripts->registered as $handle => $script ) {
		$wp_scripts->registered[ $handle ]->ver = null;
	}
}

add_action( 'wp_print_scripts', 'thim_optimize_scripts', 999 );
add_action( 'wp_print_footer_scripts', 'thim_optimize_scripts', 999 );
/**
 * Optimize style files
 */
function thim_optimize_styles() {
	global $wp_styles;
	if ( ! is_a( $wp_styles, 'WP_Styles' ) ) {
		return;
	}
	foreach ( $wp_styles->registered as $handle => $style ) {
		$wp_styles->registered[ $handle ]->ver = null;
	}
}

add_action( 'admin_print_styles', 'thim_optimize_styles', 999 );
add_action( 'wp_print_styles', 'thim_optimize_styles', 999 );


/**
 * Display favicon
 */
function thim_favicon() {

	if ( function_exists( 'wp_site_icon' ) ) {
		if ( function_exists( 'has_site_icon' ) ) {
			if ( ! has_site_icon() ) {
				// Icon default
				$thim_favicon_src = get_template_directory_uri() . "/images/favicon.png";
				echo '<link rel="shortcut icon" href="' . esc_url( $thim_favicon_src ) . '" type="image/x-icon" />';

				return;
			}

			return;
		}
	}

	/**
	 * Support WordPress < 4.3
	 */
	$theme_options_data = thim_options_data();
	$thim_favicon_src   = '';
	if ( isset( $theme_options_data['thim_favicon'] ) ) {
		$thim_favicon       = $theme_options_data['thim_favicon'];
		$favicon_attachment = wp_get_attachment_image_src( $thim_favicon, 'full' );
		$thim_favicon_src   = $favicon_attachment[0];
	}
	if ( ! $thim_favicon_src ) {
		$thim_favicon_src = get_template_directory_uri() . "/images/favicon.png";
	}
	echo '<link rel="shortcut icon" href="' . esc_url( $thim_favicon_src ) . '" type="image/x-icon" />';

}

add_action( 'wp_head', 'thim_favicon' );


/**
 * Display post thumbnail by default
 *
 * @param $size
 */
function thim_default_get_post_thumbnail( $size ) {
	if ( thim_plugin_active( 'bh-framework/tp-framework.php' ) ) {
		return;
	}
	if ( get_the_post_thumbnail( get_the_ID(), $size ) ) {
		?>
		<div class='post-formats-wrapper'>
			<a class="post-image" href="<?php echo esc_url( get_permalink() ); ?>">
				<?php echo get_the_post_thumbnail( get_the_ID(), $size, array( 'itemprop' => 'image' ) ); ?>
			</a>
		</div>
		<?php
	}
}

add_action( 'thim_entry_top', 'thim_default_get_post_thumbnail', 20 );


// Custom loading image for contact form 7
add_filter( 'wpcf7_ajax_loader', 'thim_wpcf7_ajax_loader' );
function thim_wpcf7_ajax_loader() {
	return THIM_URI . 'images/loading.gif';
}


add_action( 'thim_loading_logo', 'thim_loading_logo', 10 );
// loading logo
if ( ! function_exists( 'thim_loading_logo' ) ) :
	function thim_loading_logo() {
		$theme_options_data = get_theme_mods();
		$thim_logo_src      = get_template_directory_uri( 'template_directory' ) . '/images/loading.gif';
		if ( isset( $theme_options_data['thim_loading_logo'] ) && $theme_options_data['thim_loading_logo'] <> '' ) {
			$thim_logo_src = $theme_options_data['thim_loading_logo'];
			if ( is_numeric( $thim_logo_src ) ) {
				$logo_attachment = wp_get_attachment_image_src( $thim_logo_src, 'full' );
				$thim_logo_src   = $logo_attachment[0];
			}
		}
		echo '<img src="' . esc_url( $thim_logo_src ) . '" alt="' . esc_attr( get_bloginfo( 'name', 'display' ) ) . '" />';
	}
endif;

// Thim meta
function thim_meta_landscaping( $key, $args = array(), $post_id = null ) {
	$post_id = empty( $post_id ) ? get_the_ID() : $post_id;

	$args = wp_parse_args( $args, array(
		'type' => 'text',
	) );

	// Image
	if ( in_array( $args['type'], array( 'image' ) ) ) {
		if ( isset( $args['single'] ) && $args['single'] == 'false' ) {
			// Gallery
			$temp = array();
			$data = array();

			$attachment_id = get_post_meta( $post_id, $key, true );
			if ( ! $attachment_id ) {
				return $data;
			}
			$value = explode( ',', $attachment_id );

			if ( empty( $value ) ) {
				return $data;
			}
			foreach ( $value as $k => $v ) {
				$image_attributes = wp_get_attachment_image_src( $v, $args['size'] );
				$temp['url']      = $image_attributes[0];
				$data[]           = $temp;
			}

			return $data;
		} else {
			// Single Image
			$attachment_id    = get_post_meta( $post_id, $key, true );
			$image_attributes = wp_get_attachment_image_src( $attachment_id, $args['size'] );

			return $image_attributes;
		}
	}

	return get_post_meta( $post_id, $key, $args );
}


/**
 * BreadcrumbList
 * http://schema.org/BreadcrumbList
 */
function thim_breadcrumbs() {
	// Settings
	$separator        = '';
	$breadcrums_id    = 'thim_breadcrumbs';
	$breadcrums_class = 'thim-breadcrumbs';
	$home_title       = esc_html__( 'Home', 'landscaping' );

	// If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
	$custom_taxonomy_list = 'product_cat,dn_campaign_cat';

	// Get the query & post information
	global $post, $wp_query;
	// Do not display on the homepage
	if ( ! is_front_page() ) {
		// Build the breadcrums
		echo '<ul id="' . $breadcrums_id . '" class="' . $breadcrums_class . '" itemscope itemtype="http://schema.org/BreadcrumbList">';

		// Home page
		echo '<li class="item-home" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" class="bread-link bread-home" href="' . esc_url( home_url( '/' ) ) . '" title="' . $home_title . '"><span itemprop="name">' . $home_title . '</span></a></li>';
		echo '<li class="separator separator-home"> ' . $separator . ' </li>';

		if ( is_archive() && ! is_tax() && ! is_category() && ! is_tag() ) {

			echo '<li class="item-current item-archive" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span class="bread-current bread-archive" itemprop="name">' . esc_html__( 'All', 'landscaping' ) . ' ' . post_type_archive_title( '', false ) . '</span></li>';

		} else if ( is_archive() && is_tax() && ! is_category() && ! is_tag() ) {

			// If post is a custom post type
			$post_type = get_post_type();

			// If it is a custom post type display name and link
			if ( $post_type != 'post' ) {

				$post_type_object  = get_post_type_object( $post_type );
				$post_type_archive = get_post_type_archive_link( $post_type );

				echo '<li class="item-cat item-custom-post-type-' . $post_type . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . esc_url( $post_type_archive ) . '" title="' . $post_type_object->labels->name . '"><span itemprop="name">' . $post_type_object->labels->name . '</span></a></li>';
				echo '<li class="separator"> ' . $separator . ' </li>';

			}

			$custom_tax_name = get_queried_object()->name;
			echo '<li class="item-current item-archive" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name" class="bread-current bread-archive">' . $custom_tax_name . '</span></li>';

		} else if ( is_single() ) {

			// If post is a custom post type
			$post_type = get_post_type();


			// If it is a custom post type display name and link
			if ( $post_type != 'post' ) {

				$post_type_object  = get_post_type_object( $post_type );
				$post_type_archive = get_post_type_archive_link( $post_type );
				$post_title        = $post_type_object->labels->name;

				if ( $post_type === 'tp_event' ) {
					$post_title = esc_html__( 'All Events', 'landscaping' );
				}

				echo '<li class="item-current item-cat item-custom-post-type-' . $post_type . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . esc_url( $post_type_archive ) . '" title="' . $post_type_object->labels->name . '"><span itemprop="name">' . $post_title . '</span></a></li>';

			}

			// Get post category info
			$category = get_the_category();
			$cat_display = '';
			$cat_nicename = '';
			$cat_id = '';

			if ( ! empty( $category ) ) {

				// Get last category post is in

				// Get parent any categories and create array
				$get_cat_parents = rtrim( get_category_parents( $category[0]->term_id, true, ',' ), ',' );
				$cat_parents     = explode( ',', $get_cat_parents );

				// Loop through parent categories and store in variable $cat_display
				$cat_display = '';
				foreach ( $cat_parents as $parents ) {
					$cat_display .= '<li class="item-current item-cat" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name">' . $parents . '</span></li>';
				}

			}

			// If it's a custom post type within a custom taxonomy
			$custom_taxonomys = explode( ',', $custom_taxonomy_list );

			foreach ( $custom_taxonomys as $key => $custom_taxonomy ) {
				$taxonomy_exists = taxonomy_exists( $custom_taxonomy );
				if ( empty( $last_category ) && ! empty( $custom_taxonomy ) && $taxonomy_exists ) {
					$taxonomy_terms       = get_the_terms( $post->ID, $custom_taxonomy );
					$taxonomy_terms_first = (array) $taxonomy_terms[0];
					$cat_id               = isset( $taxonomy_terms_first['term_id'] ) ? $taxonomy_terms_first['term_id'] : '';
					$cat_nicename         = isset( $taxonomy_terms_first['slug'] ) ? $taxonomy_terms_first['slug'] : '';
					if ( isset( $taxonomy_terms_first['term_id'] ) ) {
						$cat_link = get_term_link( $taxonomy_terms_first['term_id'], $custom_taxonomy );
					}
					$cat_name = isset( $taxonomy_terms_first['name'] ) ? $taxonomy_terms_first['name'] : '';
				}
			}


			// Check if the post is in a category
			if ( ! empty( $category ) ) {
				echo ent2ncr( $cat_display );

				// Else if post is in a custom taxonomy
			} else if ( ! empty( $cat_id ) ) {

				echo '<li class="separator"> ' . $separator . ' </li>';
				echo '<li class="item-current item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . esc_url( $cat_link ) . '" title="' . $cat_name . '"><span itemprop="name">' . $cat_name . '</span></a></li>';

			} else {
				if ( $post_type != 'tp_event' ) {
					echo '<li class="separator"> ' . $separator . ' </li>';
					echo '<li class="item-current item-' . $post->ID . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name" class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</span></li>';
				}
			}

		} else if ( is_category() ) {

			// Category page
			echo '<li class="item-current item-cat" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name" class="bread-current bread-cat">' . single_cat_title( '', false ) . '</span></li>';

		} else if ( is_page() ) {

			if ( $post->post_parent ) {

				// If child page, get parents
				$anc = get_post_ancestors( $post->ID );

				// Get parents in the right order
				$anc = array_reverse( $anc );

				// Parent page loop
				$parents = '';
				foreach ( $anc as $ancestor ) {
					$parents .= '<li class="item-parent item-parent-' . $ancestor . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" class="bread-parent bread-parent-' . $ancestor . '" href="' . esc_url( get_permalink( $ancestor ) ) . '" title="' . get_the_title( $ancestor ) . '"><span itemprop="name">' . get_the_title( $ancestor ) . '</span></a></li>';
					$parents .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
				}

				// Display parent pages
				echo ent2ncr( $parents );

				// Current page
				echo '<li class="item-current item-' . $post->ID . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name" title="' . get_the_title() . '"> ' . get_the_title() . '</span></li>';

			} else {

				// Just display current page if not parents
				echo '<li class="item-current item-' . $post->ID . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name" class="bread-current bread-' . $post->ID . '"> ' . get_the_title() . '</span></li>';

			}

		} else if ( is_tag() ) {

			// Tag page

			// Get tag information
			$term_id       = get_query_var( 'tag_id' );
			$taxonomy      = 'post_tag';
			$args          = 'include=' . $term_id;
			$terms         = get_terms( $taxonomy, $args );
			$get_term_id   = $terms[0]->term_id;
			$get_term_slug = $terms[0]->slug;
			$get_term_name = $terms[0]->name;

			// Display the tag name
			echo '<li class="item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name" class="bread-current bread-tag-' . $get_term_id . ' bread-tag-' . $get_term_slug . '">' . $get_term_name . '</span></li>';

		} elseif ( is_day() ) {

			// Day archive

			// Year link
			echo '<li class="item-year item-year-' . get_the_time( 'Y' ) . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" class="bread-year bread-year-' . get_the_time( 'Y' ) . '" href="' . esc_url( get_year_link( get_the_time( 'Y' ) ) ) . '" title="' . get_the_time( 'Y' ) . '"><span itemprop="name">' . get_the_time( 'Y' ) . esc_html__( ' Archives', 'landscaping' ) . '</span></a></li>';
			echo '<li class="separator separator-' . get_the_time( 'Y' ) . '"> ' . $separator . ' </li>';

			// Month link
			echo '<li class="item-month item-month-' . get_the_time( 'm' ) . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" class="bread-month bread-month-' . get_the_time( 'm' ) . '" href="' . esc_url( get_month_link( get_the_time( 'Y' ), get_the_time( 'm' ) ) ) . '" title="' . get_the_time( 'M' ) . '"><span itemprop="name">' . get_the_time( 'M' ) . esc_html__( ' Archives', 'landscaping' ) . '</span></a></li>';
			echo '<li class="separator separator-' . get_the_time( 'm' ) . '"> ' . $separator . ' </li>';

			// Day display
			echo '<li class="item-current item-' . get_the_time( 'j' ) . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name" class="bread-current bread-' . get_the_time( 'j' ) . '"> ' . get_the_time( 'jS' ) . ' ' . get_the_time( 'M' ) . esc_html__( ' Archives', 'landscaping' ) . '</span></li>';

		} else if ( is_month() ) {

			// Month Archive

			// Year link
			echo '<li class="item-year item-year-' . get_the_time( 'Y' ) . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" class="bread-year bread-year-' . get_the_time( 'Y' ) . '" href="' . esc_url( get_year_link( get_the_time( 'Y' ) ) ) . '" title="' . get_the_time( 'Y' ) . '"><span itemprop="name">' . get_the_time( 'Y' ) . esc_html__( ' Archives', 'landscaping' ) . '</span></a></li>';
			echo '<li class="separator separator-' . get_the_time( 'Y' ) . '"> ' . $separator . ' </li>';

			// Month display
			echo '<li class="item-month item-month-' . get_the_time( 'm' ) . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name" class="bread-month bread-month-' . get_the_time( 'm' ) . '" title="' . get_the_time( 'M' ) . '">' . get_the_time( 'M' ) . esc_html__( ' Archives', 'landscaping' ) . ' </span></li>';

		} else if ( is_year() ) {

			// Display year archive
			echo '<li class="item-current item-current-' . get_the_time( 'Y' ) . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name" class="bread-current bread-current-' . get_the_time( 'Y' ) . '" title="' . get_the_time( 'Y' ) . '">' . get_the_time( 'Y' ) . esc_html__( ' Archives', 'landscaping' ) . ' </span></li>';

		} else if ( is_author() ) {

			// Auhor archive

			// Get the author information
			global $author;
			$userdata = get_userdata( $author );

			// Display author name
			echo '<li class="item-current item-current-' . $userdata->user_nicename . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name" class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '">' . esc_html__( 'Author: ', 'landscaping' ) . $userdata->display_name . '</span></li>';

		} else if ( get_query_var( 'paged' ) ) {

			// Paginated archives
			echo '<li class="item-current item-current-' . get_query_var( 'paged' ) . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name" class="bread-current bread-current-' . get_query_var( 'paged' ) . '" title="' . esc_attr__( 'Page', 'landscaping' ) . get_query_var( 'paged' ) . '">' . esc_html__( 'Page', 'landscaping' ) . ' ' . get_query_var( 'paged' ) . '</span></li>';

		} else if ( is_search() ) {

			// Search results page
			echo '<li class="item-current item-current-' . get_search_query() . '" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name" class="bread-current bread-current-' . get_search_query() . '" title="' . esc_attr__( 'Search results', 'landscaping' ) . '">' . esc_html__( 'Search results', 'landscaping' ) . '</span></li>';

		} elseif ( is_404() ) {

			// 404 page
			echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name">' . esc_html__( '404 Error', 'landscaping' ) . '</span></li>';
		} elseif ( is_home() ) {

			// Blog page
			echo '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="name">' . wp_title( '', false, '' ) . '</span></li>';
		}

		echo '</ul>';

	}
}


function thim_ssl_secure_url( $sources ) {
	$scheme = parse_url( site_url(), PHP_URL_SCHEME );
	if ( 'https' == $scheme ) {
		if ( stripos( $sources, 'http://' ) === 0 ) {
			$sources = 'https' . substr( $sources, 4 );
		}

		return $sources;
	}

	return $sources;
}

function thim_ssl_secure_image_srcset( $sources ) {
	$scheme = parse_url( site_url(), PHP_URL_SCHEME );
	if ( 'https' == $scheme ) {
		foreach ( $sources as &$source ) {
			if ( stripos( $source['url'], 'http://' ) === 0 ) {
				$source['url'] = 'https' . substr( $source['url'], 4 );
			}
		}

		return $sources;
	}

	return $sources;
}

add_filter( 'wp_calculate_image_srcset', 'thim_ssl_secure_image_srcset' );
add_filter( 'wp_get_attachment_url', 'thim_ssl_secure_url', 1000 );
add_filter( 'image_widget_image_url', 'thim_ssl_secure_url' );


function thim_remove_script_version( $src ) {
	$parts = explode( '?ver', $src );

	return $parts[0];
}

add_filter( 'script_loader_src', 'thim_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', 'thim_remove_script_version', 15, 1 );


/**
 * Remove Emoji scripts
 */
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );


function thim_post_share_inline() {
	$theme_options_data = get_theme_mods();
	$post_type          = get_post_type();

	$prefix = 'thim_archive_';
	if ( $post_type === 'tp_event' ) {
		$prefix = 'thim_event_';
	}
	$facebook  = isset( $theme_options_data[ $prefix . 'sharing_facebook' ] ) ? $theme_options_data[ $prefix . 'sharing_facebook' ] : true;
	$twitter   = isset( $theme_options_data[ $prefix . 'sharing_twitter' ] ) ? $theme_options_data[ $prefix . 'sharing_twitter' ] : true;
	$pinterest = isset( $theme_options_data[ $prefix . 'sharing_pinterest' ] ) ? $theme_options_data[ $prefix . 'sharing_pinterest' ] : true;
	$fancy     = isset( $theme_options_data[ $prefix . 'sharing_fancy' ] ) ? $theme_options_data[ $prefix . 'sharing_fancy' ] : true;
	$google    = isset( $theme_options_data[ $prefix . 'sharing_google' ] ) ? $theme_options_data[ $prefix . 'sharing_google' ] : true;

	if ( $facebook || $twitter || $pinterest || $fancy || $google ) {
		echo '<ul class="social-share thim-social-share-inline">';

		if ( $fancy ) {
			echo '<li class="fancy">
                            <script type="text/javascript" src="//fancy.com/fancyit/v2/fancyit.js" id="fancyit" async="async" data-count="right"></script>
                        </li>';
		}

		if ( $google ) {
			echo '<li class="google">
                            <script src="https://apis.google.com/js/platform.js" async></script>
                            <div class="g-plusone" data-size="medium"></div>
                        </li>';
		}

		if ( $pinterest ) {
			echo '<li class="pinterest">
                            <a data-pin-do="buttonBookmark" href="//www.pinterest.com/pin/create/button/"><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_gray_20.png" alt=""/></a>
                            <script async src="//assets.pinterest.com/js/pinit.js"></script>
                        </li>';
		}

		if ( $twitter ) {
			echo '<li class="twitter">
                            <a href="' . esc_url( get_permalink() ) . '" class="twitter-share-button">Tweet</a>
                            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?\'http\':\'https\';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+\'://platform.twitter.com/widgets.js\';fjs.parentNode.insertBefore(js,fjs);}}(document, \'script\', \'twitter-wjs\');</script>
                        </li>';
		}

		if ( $facebook ) {

			echo '<li class="facebook">
                            <div id="fb-root"></div>
                            <script>(function(d, s, id) {
                              var js, fjs = d.getElementsByTagName(s)[0];
                              if (d.getElementById(id)) return;
                              js = d.createElement(s); js.id = id;
                              js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
                              fjs.parentNode.insertBefore(js, fjs);
                            }(document, \'script\', \'facebook-jssdk\'));</script>
                            <div class="fb-share-button" data-href="' . esc_url( get_permalink() ) . '" data-layout="button_count"></div>
                        </li>';
		}

		echo '</ul>';
	}

}

add_action( 'thim_post_share_inline', 'thim_post_share_inline' );


add_action( 'customize_save', 'thim_customize_save', 1000 );

function thim_customize_save() {
	$old_options = get_theme_mod( 'thim_body_primary_color' );

	$post_data = isset( $_POST['customized'] ) ? $_POST['customized'] : false;

	if ( ! $post_data ) {
		return null;
	}

	$string_json = ( str_replace( '\\', '', $post_data ) );
	$new_options = json_decode( $string_json );
	$new_options = (array) $new_options;


	$primary_color = isset( $new_options['thim_body_primary_color'] ) ? $new_options['thim_body_primary_color'] : $old_options;
	if ( isset( $new_options['thim_header_style'] ) && $new_options['thim_header_style'] == 'style3' ) {
		set_theme_mod( 'thim_header_overlay', false );
		set_theme_mod( 'thim_header_bg_color', 'rgb(255, 255, 255)' );
		set_theme_mod( 'thim_header_text_color', 'rgb(51, 51, 51)' );
		set_theme_mod( 'thim_header_link_hover_color', $primary_color );
		set_theme_mod( 'thim_header_menu_bg_color', $primary_color );
		set_theme_mod( 'thim_header_menu_text_color', 'rgb(255, 255, 255)' );
		set_theme_mod( 'thim_header_menu_link_hover_color', 'rgb(51, 51, 51)' );
		set_theme_mod( 'thim_header_menu_line', 'line' );
		set_theme_mod( 'thim_header_line_color', 'rgb(132, 179, 46)' );
		set_theme_mod( 'thim_header_sticky_bg_color', $primary_color );
		set_theme_mod( 'thim_header_sticky_text_color', 'rgb(255, 255, 255)' );
		set_theme_mod( 'thim_header_sticky_link_hover_color', 'rgb(51, 51, 51)' );
		set_theme_mod( 'thim_header_submenu_bg_color', $primary_color );
		set_theme_mod( 'thim_header_submenu_text_color', 'rgb(255, 255, 255)' );
		set_theme_mod( 'thim_header_submenu_link_hover_color', 'rgb(51, 51, 51)' );
		set_theme_mod( 'thim_header_mobilemenu_line', 'mobile-not-line' );
		set_theme_mod( 'thim_header_mobilemenu_line_color', 'rgb(238, 238, 238)' );
	}

	if ( isset( $new_options['thim_header_style'] ) && $new_options['thim_header_style'] == 'style5' ) {
		set_theme_mod( 'thim_header_overlay', true );
		set_theme_mod( 'thim_header_bg_color', 'rgba(255, 255, 255, 0)' );
		set_theme_mod( 'thim_header_text_color', 'rgb(255, 255, 255)' );
		set_theme_mod( 'thim_header_link_hover_color', $primary_color );
		set_theme_mod( 'thim_header_menu_bg_color', $primary_color );
		set_theme_mod( 'thim_header_menu_text_color', 'rgb(255, 255, 255)' );
		set_theme_mod( 'thim_header_menu_link_hover_color', $primary_color );
		set_theme_mod( 'thim_header_menu_line', 'not-line' );
		set_theme_mod( 'thim_header_line_color', 'rgb(238, 238, 238)' );
		set_theme_mod( 'thim_header_sticky_bg_color', 'rgb(255, 255, 255)' );
		set_theme_mod( 'thim_header_sticky_text_color', 'rgb(51, 51, 51)' );
		set_theme_mod( 'thim_header_sticky_link_hover_color', $primary_color );
		set_theme_mod( 'thim_header_submenu_bg_color', 'rgb(255, 255, 255)' );
		set_theme_mod( 'thim_header_submenu_text_color', 'rgb(51, 51, 51)' );
		set_theme_mod( 'thim_header_submenu_link_hover_color', $primary_color );
		set_theme_mod( 'thim_header_mobilemenu_line', 'mobile-not-line' );
		set_theme_mod( 'thim_header_mobilemenu_line_color', 'rgb(238, 238, 238)' );
	}

	if ( isset( $new_options['thim_header_style'] ) && $new_options['thim_header_style'] == 'style2' ) {
		set_theme_mod( 'thim_header_overlay', false );
		set_theme_mod( 'thim_header_bg_color', 'rgb(255, 255, 255)' );
		set_theme_mod( 'thim_header_text_color', 'rgb(51, 51, 51)' );
		set_theme_mod( 'thim_header_link_hover_color', $primary_color );
		set_theme_mod( 'thim_header_menu_bg_color', $primary_color );
		set_theme_mod( 'thim_header_menu_text_color', 'rgb(51, 51, 51)' );
		set_theme_mod( 'thim_header_menu_link_hover_color', $primary_color );
		set_theme_mod( 'thim_header_menu_line', 'not-line' );
		set_theme_mod( 'thim_header_line_color', 'rgb(238, 238, 238)' );
		set_theme_mod( 'thim_header_sticky_bg_color', 'rgb(255, 255, 255)' );
		set_theme_mod( 'thim_header_sticky_text_color', 'rgb(51, 51, 51)' );
		set_theme_mod( 'thim_header_sticky_link_hover_color', $primary_color );
		set_theme_mod( 'thim_header_submenu_bg_color', 'rgb(255, 255, 255)' );
		set_theme_mod( 'thim_header_submenu_text_color', 'rgb(51, 51, 51)' );
		set_theme_mod( 'thim_header_submenu_link_hover_color', $primary_color );
		set_theme_mod( 'thim_header_mobilemenu_line', 'mobile-not-line' );
		set_theme_mod( 'thim_header_mobilemenu_line_color', 'rgb(238, 238, 238)' );
	}
}

/**
 * @param $plugin
 *
 * @return bool
 */
function thim_plugin_active( $plugin ) {
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	if ( is_plugin_active( $plugin ) ) {
		return true;
	}

	return false;
}