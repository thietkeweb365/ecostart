<?php
class Alt_Sales_Countdown extends WSCD{
    public function __construct() {

        add_filter( 'woosalescountdown_tab_options', array($this, 'bh_woosalescountdown_tab_options'), 10, 1 );
        add_action( 'woocommerce_before_single_product', array( $this, 'bh_single_product' ) );
        add_action( 'woocommerce_before_shop_loop_item', array( $this, 'bh_categories_product' ) );


        add_action( 'woocommerce_update_options_woosalescountdown', array( $this, 'bh_update_options' ) );
        add_filter( "woocommerce_admin_settings_sanitize_option__woosale_date_finish", array($this, 'woosale_date_finish'), 10, 3 );


    }


    /**
     * Update plugin options.
     *
     * @return void
     * @since 1.0.0
     */
    public function bh_update_options() {
        $this->options['general'] = array(array(
            'title'   => __( 'Total product discount:', 'woosalescountdown' ),
            'id'      => '_woosale_date_finish',
            'default' => '',
            'type'    => 'text',
            'desc'    => __( '', 'woosalescountdown' ),
        ));

        foreach ( $this->options as $option ) {
            woocommerce_update_options( $option );
        }
    }

    /**
     * @param $value
     * @param $option
     * @param $raw_value
     * @return int
     */
    function woosale_date_finish($value, $option, $raw_value){
        $second = get_option('_woosale_date_interval') * 86400;
        return time() + $second;

    }

    function bh_single_product(){


        global $wp_query;
        ob_start();
        $this->single_product();
        $output = ob_get_clean();

        $ob_set_all_option = get_option('ob_set_all_option');

        if(!$output && $ob_set_all_option){

            global $wp_query;
            @$check_showon = get_option( 'ob_where_show' );
            @$show_message = get_option( 'ob_message_show', 1 );
            @$show_datetext = get_option( 'ob_datetext_show', 1 );
            $date_text    = $time_text = '';
            $localization = ', localization:{ days: "' . __( 'days', 'woosalescountdown' ) . '", hours: "' . __( 'hours', 'woosalescountdown' ) . '", minutes: "' . __( 'minutes', 'woosalescountdown' ) . '", seconds: "' . __( 'seconds', 'woosalescountdown' ) . '" }';
            if ( !$show_datetext ) {
                $date_text = ', showText:0';
            }
            if ( $check_showon == 1 ) {
                return;
            }
            if ( is_admin() || !$wp_query->post->ID ) {
                return;
            }

            $_product            = get_product( $wp_query->post->ID );
            $_turn_off_countdown = get_post_meta( $_product->id, '_turn_off_countdown', true );
            $hide_countdown      = get_post_meta( $_product->id, "_hide_only_countdown", true );
            $hide_salebar        = get_post_meta( $_product->id, "_hide_only_salebar", true );
            $not_repeat          = get_post_meta( $_product->id, "_not_repeat_countdown", true );

            if ( $_turn_off_countdown == 'yes' ) {
                return;
            }
            $gmt_off = get_option( 'gmt_offset' ) ? get_option( 'gmt_offset' ) : 0;

            $date_finish         = get_option('_woosale_date_finish');

            if ( trim( $_product->product_type ) != 'variable' ) {
                $time_from     = get_post_meta( $wp_query->post->ID, "_sale_price_dates_from", true );
                $time_end      = get_post_meta( $wp_query->post->ID, "_sale_price_dates_to", true );


                $_woosale_from_time     = get_post_meta( $wp_query->post->ID, "_woosale_from_time", true );
                $_woosale_to_time      = get_post_meta( $wp_query->post->ID, "_woosale_to_time", true );
                $discount      = get_option("ob_set_quantity_discount" );
                $sale          = get_option( "ob_set_quantity_sale" );
                $stock         = get_post_meta( $wp_query->post->ID, "_stock", true );
                $_manage_stock = get_post_meta( $wp_query->post->ID, "_manage_stock", true );

                if ( $_manage_stock ) {
                    if ( trim( $_manage_stock ) == 'yes' ) {
                        if ( $stock < 1 ) {
                            $discount = 0;
                        }
                    }
                }
                if ( !$sale ) {
                    $sale = 0;
                }

                if ( empty($date_finish) && empty($_woosale_to_time)) {
                    return;
                }
                if ( $_woosale_from_time != '' && $time_from != '' ){
                    $time_from = $this->add_specified_time( $time_from, $_woosale_from_time );
                }
                if ( $_woosale_to_time != '' &&  $time_end != ''){
                    $time_end = $this->add_specified_time( $time_end, $_woosale_to_time );
                }
                $message      = '';
                $current_time = strtotime( current_time( "Y-m-d G:i:s" ) );
                $date_format  = get_option( 'date_format', 'F j, Y' );//echo'<pre>';print_r(date('Y-m-d H:i:s',$time_from));echo'<pre>';print_r(date('Y-m-d H:i:s',$current_time));die;
                if ( $current_time < $time_from ) {
                    $time = $time_from; // - time();
                    if ( $show_message ) {
                        @$message = '<h3>' . esc_html( __( get_option( 'ob_title_coming', 'Coming' ), 'woosalescountdown' ) ) . '</h3>';
                    }
                } else {
                    $time = $time_end; // - time();
                    if ( $show_message ) {
                        @$message = '<h3>' . esc_html( __( get_option( 'ob_title_sale', 'Sale' ), 'woosalescountdown' ) ) . '</h3>';
                    }
                }

                if($current_time >= $date_finish){
                    if($not_repeat == 'yes') return;
                    $date_interval = get_option('_woosale_date_interval');
                    $date_finish = $this->cal_date_finish($date_interval);
                    $time_interval = $_woosale_to_time;
                    if(!empty($time_interval) && $time_interval != ''){
                        $date_finish = $date_finish + $this->goto_seconds($time_interval);
                    }
                    update_option( '_woosale_date_finish', $date_finish );
                    $time = $date_finish;
                }else{
                    $time = $date_finish;
                }

                $check_sale = 1;
                if ( $sale < $discount ) {
                    @$per_sale = intval( $sale / $discount * 100 );
                } else {
                    $check_sale = 0;
                }
                @$ob_time_show = get_option( 'ob_time_show', '' );




                switch ( $ob_time_show ) {
                    case 1:
                        if ( $hide_countdown != 'yes' ) {
                            $show = "<div class=\"ob_wrapper ob_product_detal\">" . $message . " <div class=\"myCounter widget_product_detail\"></div></div>";
                        }
                        break;
                    case 2:
                        if ( $check_sale && $hide_salebar != 'yes' ) {
                            $show = "<div class=\"ob_wrapper ob_product_detal\">" . $message . "  <span>" . $sale . '/' . $discount . __( ' sold', 'woosalescountdown' ) . "</span> <div class=\"ob_discount\"><div class=\"ob_sale\" style=\"width:" . $per_sale . "%\"></div></div></div>";
                        } else {
                            $show = '';
                        }

                        break;
                    case 3:
                        $show = "<div class=\"ob_wrapper ob_product_detal\">" . $message . $time_text . "</div>";

                        break;
                    default:
                        $show = "<div class=\"ob_wrapper ob_product_detal\">" . $message . "";
                        if ( $hide_salebar != 'yes' ) {
                            if ( $check_sale ) {
                                $show .= "<div class=\"ob_discount\"><div class=\"ob_sale\" style=\"width:" . $per_sale . "%\"></div></div><span>" . $sale . '/' . $discount . __( ' sold', 'woosalescountdown' ) . "</span> ";
                            }
                        }
                        if ( $hide_countdown != 'yes' ) {
                            $show .= "  <div class=\"myCounter widget_product_detail\"></div>";
                        }
                        $show .= "</div>";
                }
                @$countdown_position = get_option( 'ob_detail_position', 0 );

                /*Clear Cache*/
                $this->clear_cache();

                if ( $show ) {
                    switch ( $countdown_position ) {
                        case 1:
                            echo "<script type='text/javascript'>
						jQuery(document).ready(function(){
							jQuery('" . $show . "').insertAfter('.woocommerce-tabs');
						});
					</script>
					";
                            break;
                        case 2:
                            echo "<script type='text/javascript'>
						jQuery(document).ready(function(){
							jQuery('div[itemprop=\"description\"]').each(function (index, value) {
								if (index == 0){
									jQuery('" . $show . "').insertBefore(jQuery(this));
								}
							});
						});
					</script>
					";
                            break;
                        case 3:
                            echo "<script type='text/javascript'>
						jQuery(document).ready(function(){
							jQuery('div[itemprop=\"description\"]').each(function (index, value) {
								if (index == 0){
									jQuery('" . $show . "').insertAfter('div[itemprop=\"description\"]');
								}
							});
						});
					</script>
					";
                            break;
                        case
                        4:
                            echo "<script type='text/javascript'>
						jQuery(document).ready(function(){
							jQuery('" . $show . "').insertBefore('form.cart');
						});
					</script>
					";
                            break;
                        case 5:
                            echo "<script type='text/javascript'>
						jQuery(document).ready(function(){
							jQuery('" . $show . "').insertAfter('form.cart');
						});
					</script>
					";
                            break;
                        default:
                            echo "<script type='text/javascript'>
				jQuery(document).ready(function(){
					jQuery('" . $show . "').insertBefore('.woocommerce-tabs');
				});
			</script>
			";

                    }

                    echo "<script type='text/javascript'>
			jQuery(function () {
				jQuery('.myCounter').mbComingsoon({ expiryDate: new Date(" . date( "Y", $time ) . ", " . ( date( "m", $time ) - 1 ) . ", " . date( "d", $time ) . ", " . date( "G", $time ) . ", " . date( "i", $time ) . ", " . date( "s", $time ) . "), speed:500, gmt:" . $gmt_off . $date_text . $localization . " });
			});
			</script>";
                }

                /* End: Not var */
            }


        }

    }
    function bh_categories_product(){
        global $post;
        ob_start();
        $this->categories_product();
        $output = ob_get_clean();

        $ob_set_all_option = get_option('ob_set_all_option');

        if(!$output && $ob_set_all_option){
            $old_id = $product_id = $post->ID;
            @$check_showon = get_option( 'ob_where_show' );
            @$show_message = get_option( 'ob_message_show', 1 );
            @$show_datetext = get_option( 'ob_datetext_show', 1 );
            $date_text    = '';
            $localization = ', localization:{ days: "' . __( 'days', 'woosalescountdown' ) . '", hours: "' . __( 'hours', 'woosalescountdown' ) . '", minutes: "' . __( 'minutes', 'woosalescountdown' ) . '", seconds: "' . __( 'seconds', 'woosalescountdown' ) . '" }';
            if ( !$show_datetext ) {
                $date_text = ', showText:0';
            }
            if ( $check_showon == 2 ) {
                return;
            }
            if ( is_admin() || !$post->ID ) {
                return;
            }
            $product_id = $post->ID;

            $wpml_id = get_post_meta( $product_id, '_wcml_duplicate_of_variation', true );
            $old_id  = $product_id;
            if ( $wpml_id ) {
                $product_id = $wpml_id;
            }
            $product             = get_product( $product_id );
            $_turn_off_countdown = get_post_meta( $product_id, '_turn_off_countdown', true );
            $hide_countdown      = get_post_meta( $product_id, "_hide_only_countdown", true );
            $hide_salebar        = get_post_meta( $product_id, "_hide_only_salebar", true );
            $not_repeat          = get_post_meta( $product_id, "_not_repeat_countdown", true );

            if ( $_turn_off_countdown == 'yes' ) {
                return;
            }
            $gmt_off = get_option( 'gmt_offset' ) ? get_option( 'gmt_offset' ) : 0;
            if ( $product->product_type == 'variable' ) {

            }else{
                $time_from     = get_post_meta( $product_id, "_sale_price_dates_from", true );
                $time_end      = get_post_meta( $product_id, "_sale_price_dates_to", true );

                $date_finish         = get_option('_woosale_date_finish');
                $_woosale_from_time     = get_post_meta( $product_id, "_woosale_from_time", true );
                $_woosale_to_time      = get_post_meta( $product_id, "_woosale_to_time", true );
                $discount      = get_option("ob_set_quantity_discount" );
                $sale          = get_option( "ob_set_quantity_sale" );
                $stock         = get_post_meta( $product_id, "_stock", true );
                $_manage_stock = get_post_meta( $product_id, "_manage_stock", true );
            }

            if ( count( $_manage_stock ) > 0 ) {
                if ( trim( $_manage_stock ) == 'yes' ) {
                    if ( $stock < 1 ) {
                        $discount = 0;
                    }
                }
            }
            if ( !$sale ) {
                $sale = 0;
            }

            if ( empty($date_finish) && empty($_woosale_to_time) ) {
                return;
            }
            if ( $_woosale_from_time != '' && $time_from != '' ){
                $time_from = $this->add_specified_time( $time_from, $_woosale_from_time );
            }
            if ( $_woosale_to_time != '' && $time_end != '' ){
                $time_end = $this->add_specified_time( $time_end, $_woosale_to_time );
            }
            $message      = '';
            $current_time = strtotime( current_time( "Y-m-d G:i:s" ) );
            $date_format  = get_option( 'date_format', 'F j, Y' );
            if ( $current_time < $time_from ) {
                $time = $time_from; // - time();
                if ( $show_message ) {
                    @$message = '<h3>' . esc_html( __( get_option( 'ob_title_coming', 'Coming' ), 'woosalescountdown' ) ) . '</h3>';
                }
            } else {
                $time = $time_end; // - time();
                if ( $show_message ) {
                    @$message = '<h3>' . esc_html( __( get_option( 'ob_title_sale', 'Sale' ), 'woosalescountdown' ) ) . '</h3>';
                }
            }

            if($current_time >= $date_finish){
                if($not_repeat == 'yes') return;
                $date_interval = get_option('_woosale_date_interval');
                $date_finish = $this->cal_date_finish($date_interval);
                $time_interval = $_woosale_to_time;
                if(!empty($time_interval) && $time_interval != ''){
                    $date_finish = $date_finish + $this->goto_seconds($time_interval);
                }
                update_option('_woosale_date_finish', $date_finish );
                $time = $date_finish;
            }else{
                $time = $date_finish;
            }

            $check_sale = 1;
            if ( $sale < $discount ) {
                @$per_sale = intval( $sale / $discount * 100 );
            } else {
                $check_sale = 0;
            }
            @$ob_time_show = get_option( 'ob_time_show' );
            switch ( $ob_time_show ) {
                case 1:
                    if ( $hide_countdown != 'yes' ) {
                        $show = "<div class=\"ob_wrapper ob_categories\">" . $message . " <div class=\"myCounter_pro" . $product_id . " widget_product_loop\"></div></div>";
                    }
                    break;
                case 2:
                    if ( $check_sale && $hide_salebar != 'yes' ) {
                        $show = "<div class=\"ob_wrapper ob_categories\">" . $message . " <div class=\"ob_discount\"><div class=\"ob_sale\" style=\"width:" . $per_sale . "%\"></div></div><span>" . $sale . '/' . $discount . __( ' sold', 'woosalescountdown' ) . "</span></div>";
                    }
                    break;
                case 3:
                    $show = "<div class=\"ob_wrapper ob_categories\">" . $message . $time_text . "</div>";
                    break;
                default:
                    $show = "<div class=\"ob_wrapper ob_categories\">" . $message;
                    if ( $check_sale && $hide_salebar != 'yes' ) {
                        $show .= " <div class=\"ob_discount\"><div class=\"ob_sale\" style=\"width:" . $per_sale . "%\"></div></div><span>" . $sale . '/' . $discount . __( ' sold', 'woosalescountdown' ) . "</span>";
                    }
                    if ( $hide_countdown != 'yes' ) {
                        $show .= " <div class=\"myCounter_pro" . $product_id . " widget_product_loop\"></div>";
                    }
                    $show .= "</div>";
            }

            @$countdown_position = get_option( 'ob_category_position', 0 );


            @$ob_time_show = get_option( 'ob_time_show' );
            switch ( $ob_time_show ) {
                case 1:
                    if ( $hide_countdown != 'yes' ) {
                        $show = "<div class=\"ob_wrapper ob_categories\">" . $message . " <div class=\"myCounter_pro" . $product_id . " widget_product_loop\"></div></div>";
                    }
                    break;
                case 2:
                    if ( $check_sale && $hide_salebar != 'yes' ) {
                        $show = "<div class=\"ob_wrapper ob_categories\">" . $message . " <div class=\"ob_discount\"><div class=\"ob_sale\" style=\"width:" . $per_sale . "%\"></div></div><span>" . $sale . '/' . $discount . __( ' sold', 'woosalescountdown' ) . "</span></div>";
                    }
                    break;
                case 3:
                    $show = "<div class=\"ob_wrapper ob_categories\">" . $message . $time_text . "</div>";
                    break;
                default:
                    $show = "<div class=\"ob_wrapper ob_categories\">" . $message;
                    if ( $check_sale && $hide_salebar != 'yes' ) {
                        $show .= " <div class=\"ob_discount\"><div class=\"ob_sale\" style=\"width:" . $per_sale . "%\"></div></div><span>" . $sale . '/' . $discount . __( ' sold', 'woosalescountdown' ) . "</span>";
                    }
                    if ( $hide_countdown != 'yes' ) {
                        $show .= " <div class=\"myCounter_pro" . $product_id . " widget_product_loop\"></div>";
                    }
                    $show .= "</div>";
            }

            @$countdown_position = get_option( 'ob_category_position', 0 );
            /*Clear Cache*/
            $this->clear_cache();
            switch ( $countdown_position ) {
                case 1:
                    echo "
						<script type='text/javascript'>
							jQuery(document).ready(function(){
							    if(jQuery('li.post-" . $old_id . " .ob_categories').length < 1){
									jQuery('" . $show . "').insertBefore('li.post-" . $old_id . " h3');
								}
							});
						</script>
						";
                    break;
                case 2:
                    echo "
					<script type='text/javascript'>
						jQuery(document).ready(function(){
						if(jQuery('li.post-" . $old_id . " .ob_categories').length < 1){
							jQuery('" . $show . "').insertBefore('li.post-" . $old_id . " a.add_to_cart_button');
							}
						});
					</script>
					";
                    break;
                case 3:
                    echo "
					<script type='text/javascript'>
						jQuery(document).ready(function(){
						if(jQuery('li.post-" . $old_id . " .ob_categories').length < 1){
							jQuery('" . $show . "').insertAfter('li.post-" . $old_id . " a.add_to_cart_button');
							}
						});
					</script>
					";
                    break;
                default :
                    echo "
					<script type='text/javascript'>
						jQuery(document).ready(function(){
						if(jQuery('li.post-" . $old_id . " .ob_categories').length < 1){
							jQuery('" . $show . "').insertBefore('li.post-" . $old_id . " span.price');
							}
						});
					</script>
					";
            }
            echo "<script type='text/javascript'>
		jQuery(function () {
			jQuery('.myCounter_pro" . $product_id . "').mbComingsoon({ expiryDate: new Date(" . date( "Y", $time ) . ", " . ( date( "m", $time ) - 1 ) . ", " . date( "d", $time ) . ", " . date( "G", $time ) . ", " . date( "i", $time ) . ", " . date( "s", $time ) . "), speed:500,gmt: " . $gmt_off . $date_text . $localization . " });
		});
		</script>";
        }


    }

    function bh_woosalescountdown_tab_options($options){

        $new_options = array(
            array( 'title' => __( 'Set All for Sales Countdown', 'woosalescountdown' ),
                'type'  => 'title',
                'desc'  => '',
                'id'    => 'wooscd_product_set_all' ),
            array(
                'title'   => __( 'Set All', 'woosalescountdown' ),
                'id'      => 'ob_set_all_option',
                'default' => '0',
                'type'    => 'radio',
                'desc'    => __( 'Do you want to set all product?', 'woosalescountdown' ),
                'options' => array(
                    '0' => __( 'No', 'woosalescountdown' ),
                    '1' => __( 'Yes', 'woosalescountdown' )
                ),
            ),
            array(
                'title'   => __( 'Date Interval', 'woosalescountdown' ),
                'id'      => '_woosale_date_interval',
                'default' => '',
                'type'    => 'text',
                'placeholder' => __('Điền số ngày muốn repeat. Ví dụ: 3', 'woosalescountdown'),
                'desc_tip'    => __( 'Enter number of date need to repeat', 'woosalescountdown' ),
            ),
            array(
                'title'   => __( 'Total product discount:', 'woosalescountdown' ),
                'id'      => 'ob_set_quantity_discount',
                'default' => '',
                'type'    => 'text',
                'desc'    => __( '', 'woosalescountdown' ),
            ),
            array(
                'title'   => __( 'Quantity product sale:', 'woosalescountdown' ),
                'id'      => 'ob_set_quantity_sale',
                'default' => '',
                'type'    => 'text',
                'desc'    => __( '', 'woosalescountdown' ),
            ),
            array( 'type' => 'sectionend', 'id' => 'wooscd_product_set_all' )
        );

        $options['general'] = array_merge($options['general'], $new_options);

        return $options;
    }


}
new Alt_Sales_Countdown();

