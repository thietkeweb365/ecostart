<?php
/**
 * Sidebar
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/sidebar.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
} ?>
<div class="col-md-3">
	<div class="custom-block custom-block-1">
		<div>
			<i class="icon-shipped"></i><h3>FREE<br>SHIPPING</h3>
		</div>
		<div>
			<i class="icon-us-dollar"></i><h3>100% MONEY<br>BACK GUARANTEE</h3>
		</div>
		<div>
			<i class="icon-online-support"></i><h3>ONLINE<br>SUPPORT 24/7</h3>
		</div>
	</div>

    <div id="bh-sidebar-single">
        <?php dynamic_sidebar('sidebar-product');?>
    </div>
</div>