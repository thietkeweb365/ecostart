<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see     http://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.1
 */


if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


global $product, $woocommerce_loop, $wp_query;
$theme_options_data = get_theme_mods();

?>
<li <?php post_class( $classes ); ?>>
	<div class="content__product">
		<?php do_action( 'woocommerce_before_shop_loop_item' );
        if($theme_options_data['thim_woo_type'] != 'default'){?>
        <a class="product-loop-title" href="<?php echo esc_url( get_the_permalink() ); ?>" title="<?php echo $post->post_title;?>"><h3 style="font-size:<?php echo $theme_options_data['thim_woo_title_product_size'];?>"><?php echo $post->post_title;?></h3></a>
        <?php }
        /**
         * woocommerce_before_shop_loop_item_title hook.
         *
         * @hooked woocommerce_show_product_loop_sale_flash - 10
         * @hooked woocommerce_template_loop_product_thumbnail - 10
         */
        do_action( 'woocommerce_before_shop_loop_item_title' );
        if($theme_options_data['thim_woo_type'] == 'default'){
		?>
        <a class="product-loop-title" href="<?php echo esc_url( get_the_permalink() ); ?>" title="<?php echo $post->post_title;?>"><h3 style="font-size:<?php echo $theme_options_data['thim_woo_title_product_size'];?>"><?php echo $post->post_title;?></h3></a>
        <?php
        }
        /**
         * woocommerce_after_shop_loop_item_title hook
         *
         * @hooked woocommerce_template_loop_rating - 5
         * @hooked woocommerce_template_loop_price - 10
         */
        do_action( 'woocommerce_after_shop_loop_item_title' );

        /**
         * woocommerce_after_shop_loop_item hook.
         *
         * @hooked woocommerce_template_loop_product_link_close - 5
         * @hooked woocommerce_template_loop_add_to_cart - 10
         */
        do_action( 'woocommerce_after_shop_loop_item' );
        ?>



	</div>
</li>
