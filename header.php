<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package thim
 */
$theme_options_data = get_theme_mods();
?>
<!DOCTYPE html>
<html <?php language_attributes();
if ( isset( $theme_options_data['thim_rtl_support'] ) && $theme_options_data['thim_rtl_support'] == '1' ) {
    echo " dir=\"rtl\"";
} ?> itemscope="itemscope" itemtype="http://schema.org/WebPage">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <title itemprop="name"><?php
       global $page, $paged;
       $site_description = get_bloginfo( 'description', 'display' );
       if(is_home() || is_front_page()){
             echo $site_description;
       }else{
          wp_title( '»', true, 'right' );
       }
       ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
    <?php wp_head();?>
</head>
<body <?php echo body_class();?>>
<div id="wrapper-container">
    <header id="masthead" class="site-header">
        <div class="header-top">
            <div class="container">
                <div class="header-left">
                <?php if( is_home() || get_option('page_on_front') == $post->ID ):?>
                    <h1 class="pull-left" style="margin: 15px 0 16px;">
                        <?php echo $theme_options_data['thim_seo_h1'];?>
                    </h1>
                <?php else:?>
                    <p class="pull-left">
                        <?php echo $theme_options_data['thim_seo_h1'];?>
                    </p>
                    <?php
                    if($theme_options_data['thim_infor_address']){?>
                    <p class="pull-right">
                        <i class="fa fa-map-marker"></i> <?php echo $theme_options_data['thim_infor_address'];?>
                    </p>
                    <?php }?>
                <?php endif;?>
                </div>
            </div><!-- .container -->
        </div><!-- .header-top -->

        <div class="header-main">
            <div class="container">
                <div class="header-left">

                    <div class="logo logo-transition">
                        <a href="<?php echo home_url();?>" title="<?php echo get_bloginfo('name');?>" rel="home"><img class="img-responsive standard-logo" src="<?php echo get_src($theme_options_data['thim_logo'], 'full');?>" alt="<?php echo get_bloginfo('name');?>"></a>
                    </div>
                </div><!-- .header-left -->

                <div class="header-right">
                    <div class="header-contact">
                        <ul class="header-extra-info">
                            <li>
                                <div class="feature-box feature-box-style-3">
                                    <div class="feature-box-icon">
                                        <i class="fa fa-phone text-color-primary"></i>
                                    </div>
                                    <div class="feature-box-info">
                                        <h4 class="m-b-none text-color-dark"><?php echo $theme_options_data['thim_infor_tel'];?></h4>
                                        <p><small><?php echo $theme_options_data['thim_infor_tel_desc'];?></small></p>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="feature-box feature-box-style-3">
                                    <div class="feature-box-icon">
                                        <i class="fa fa-envelope text-color-primary"></i>
                                    </div>
                                    <div class="feature-box-info">
                                        <h4 class="m-b-none text-color-dark"><?php echo $theme_options_data['thim_infor_email'];?></h4>
                                        <p><small><?php echo $theme_options_data['thim_infor_email_desc'];?></small></p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div><!-- .header-right -->
            </div><!-- .container -->
        </div><!-- .header-main -->

        <div class="main-menu-wrap">
            <div id="main-menu" class="container ">
                <div class="menu-center">
                    <div class="mobi-menu">
                        <div class="hamburger hamburger--slider-r">
                            <div class="hamburger-box">
                                <div class="hamburger-inner"></div>
                            </div>
                        </div>
                    </div>
                    <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'menu-main-menu', 'menu_class' => 'main-menu mega-menu', 'container' => '' ) ); ?>
                </div>
            </div><!-- #main-menu -->
        </div><!-- .main-menu-wrap -->
    </header>

    <main id="main-wrapper">
