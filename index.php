<?php
get_header();
$blog = get_post(get_option('page_for_posts'));
$home = get_post(get_option( 'page_on_front' ));?>
    <div class="breadcrumbs-wrap">
        <div class="container">
            <ol itemscope="" itemtype="http://schema.org/BreadcrumbList">
                <li><a href="<?php echo home_url();?>"><?php echo $home->post_title;?></a></li>&nbsp;/&nbsp;
                <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a href="<?php echo get_permalink($blog->ID);?>" itemscope="" itemtype="http://schema.org/Thing" itemprop="item"><span itemprop="name"><?php echo $blog->post_title;?></span></a><meta itemprop="position" content="1"></li>
            </ol>
        </div>
    </div>
    <div class="container" id="content">
        <div class="row">
            <div class="col-md-9">
                <h1 class="heading__primary"><span class="inline-title"><?php echo $blog->post_title?></span><span class="line"></span></h1>
                <div class="blog-posts">
                    <?php
                    $posts_per_page = get_option( 'posts_per_page' );
                      $query_args = array(
                        'post_status' => 'publish',
                        'post_type' => 'post',
                        'posts_per_page' => $posts_per_page,
                        'orderby' => 'date',
                        'order' => 'DESC',
                          'paged' => max($paged, $page)
                      );

                      $query_posts = new WP_Query($query_args);
                      if( $query_posts->have_posts() ){
                        while ($query_posts->have_posts()) : $query_posts->the_post();
                            get_template_part( 'templates/template-parts/content' );
                        endwhile;
                      }
                        wp_reset_postdata();
                      ?>
                </div>
                <?php wp_pagenavi( array( 'query' => $query_posts ) ); ?>
            </div>
            <?php get_sidebar();?>
        </div>
    </div>
<?php get_footer();?>