<?php
get_header();
$cats = get_the_category();
$home = get_post(get_option( 'page_on_front' ));
if($cats){
    $breadcrumbs = '';
    foreach ($cats as $key => $cat) {
        $breadcrumbs .= '<li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a href="'.get_category_link($cat->term_id).'" itemscope="" itemtype="http://schema.org/Thing" itemprop="item"><span itemprop="name">'.$cat->name.'</span></a><meta itemprop="position" content="'.($key+1).'"></li>&nbsp;/&nbsp;';
    }
    $breadcrumbs = trim($breadcrumbs);


}?>
    <div class="breadcrumbs-wrap">
        <div class="container">
            <ol itemscope="" itemtype="http://schema.org/BreadcrumbList">
                <li><a href="<?php echo home_url();?>"><?php echo $home->post_title;?></a></li>&nbsp;/&nbsp;
                <?php echo $breadcrumbs;?>
                <li><?php echo $post->post_title;?></li>
            </ol>
        </div>
    </div>
    <div class="container" id="content">
        <div class="row">
            <div class="col-md-9">
                <?php
                while ( have_posts() ) : the_post(); ?>
                    <?php get_template_part( 'templates/template-parts/content', 'single' ); ?>
                    <?php
                    // If comments are open or we have at least one comment, load up the comment template
                    if ( comments_open() || '0' != get_comments_number() ) :
                        comments_template();
                    endif;
                    ?>
                <?php endwhile; ?>
            </div>
            <?php get_sidebar();?>
        </div>
    </div>
<?php get_footer();?>