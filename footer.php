<?php $theme_options_data = get_theme_mods();?>
    </div><!-- #wrapper-container -->
    <footer id="footer">
        <div id="top-footer">
            <div class="container">
                <div class="row">
                    <?php dynamic_sidebar('footer');?>




                    <div class="col-md-4 footer-widgets social-footer">
                        <h3 class="widget-title">Newsletter</h3>
                        <div class="cf7-newsletter">
                            <input type="email" name="EMAIL" placeholder="Your email here" required="">
                            <input type="submit" value="Submit">
                        </div>
                        <ul class="social_link">
                            <?php if($theme_options_data['thim_social_facebook']){?>
                            <li><a class="facebook" href="<?php echo $theme_options_data['thim_social_facebook'];?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <?php }
                            if($theme_options_data['thim_social_twitter']){?>
                            <li><a class="twitter" href="<?php echo $theme_options_data['thim_social_twitter'];?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <?php }
                            if($theme_options_data['thim_social_google']){?>
                            <li><a class="digg" href="<?php echo $theme_options_data['thim_social_google'];?>" target="_blank"><i class="fa fa-google"></i></a></li>
                            <?php }
                            if($theme_options_data['thim_social_youtube']){?>
                            <li><a class="youtube" href="<?php echo $theme_options_data['thim_social_youtube'];?>" target="_blank"><i class="fa fa-youtube"></i></a></li>
                            <?php }?>
                            </ul>
                    </div>
                </div>
            </div>
        </div><!-- #top-footer -->

        <div id="bottom-footer">
            <div class="container clearfix">
                <p>© Copyright 2016. All Rights Reserved.</p>
                <?php wp_nav_menu( array( 'theme_location' => 'footer', 'menu_id' => 'menu-footer-links', 'menu_class' => 'menu', 'container' => '' ) ); ?>
            </div>
        </div>

    </footer>
    <?php wp_footer();?>
</body>
</html>