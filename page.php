<?php
the_post();
get_header();?>
    <div class="breadcrumbs-wrap">
        <div class="container">
            <ol itemscope="" itemtype="http://schema.org/BreadcrumbList">
                <li><a href="<?php echo home_url();?>">Home</a></li>&nbsp;/&nbsp;<li><?php echo $post->post_title;?></li></ol></div></div>
    <div class="container" id="content">
        <h1 class="heading__primary"><span class="inline-title"><?php the_title();?></span><span class="line" style="left: 97px; width: 678px;"></span></h1>
        <?php the_content();?>
    </div>
<?php get_footer();?>